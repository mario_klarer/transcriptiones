<x-layouts.app-layout>
    <x-slot:styles>
        <x-essentials.style :styles="['/css/explanation.css']"></x-essentials.style>
    </x-slot:styles>

    <x-slot:content>
        <div class="index-main-content-wrap">
            <div class="container">
                <div class="row">
                    <div class="offset-sm-1 col-sm-10 card">
                        <div class="card-body">
                            <h1>Funktionsweise</h1>
                            <div class="documentation-textblock">Eine Seite auf Transcriptiones ist in drei Teile aufgeteilt:</div>
                            <div class="documentation-blocks-wrap">
                                <a href="#navigation" class="documentation-block documentation-block-nav">
                                    <div class="documentation-block-flex">
                                        <div class="documentation-block-descriptor">Navigation</div>
                                    </div>
                                </a>
                                <div class="documentation-block-side-wrap">
                                    <a href="#left" class="documentation-block documentation-block-side documentation-block-left">
                                        <div class="documentation-block-flex">
                                            <div class="documentation-block-descriptor">Transkriptionsseite</div>
                                        </div>
                                    </a>
                                    <a href="#right" class="documentation-block documentation-block-side documentation-block-right">
                                        <div class="documentation-block-flex">
                                            <div class="documentation-block-descriptor">Manuskriptseite</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <h2 id="navigation">Navigation</h2>
                            <div class="documentation-textblock">Die Navigationsleiste dient zur einfachen Navigation durch die transkribierten Texte. Zudem bietet die Navigationsleiste die Möglichkeit, einzelne Wörter auf der geöffneten Seite oder im gesamten Text zu suchen.</div>
                            <table class="documentation-table">
                                <tbody>
                                <tr class="documentation-tablerow">
                                    <th class="documentation-tablecol documentation-tablecol-header">Funktion</th>
                                    <th class="documentation-tablecol documentation-tablecol-header">Erklärung</th>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function">
                                        <div class="input-group input-group-sm">
                                            <input id="search-live-text" name="value" type="text" placeholder="Suchterm" autocomplete="off" class="form-control">
                                            <button v-tooltip="'Suche nach ganzen Wörtern'" class="btn btn-outline-primary">W</button>
                                            <button v-tooltip="'Innerhalb der Seite suchen'" class="btn btn-outline-primary">
                                                <span class="bi bi-search"></span>
                                            </button>
                                            <button v-tooltip="'Ganzen Text durchsuchen'" class="btn btn-outline-primary">
                                                <span class="bi bi-book"></span>
                                                <i class="bi bi-search page-navigation-bar__full-search-icon"></i>
                                            </button>
                                        </div>
                                    </td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Suchfunktion<br>Mit dieser Funktion können Sie entweder innerhalb der geöffneten Seite nach Wörtern suchen, indem Sie auf <span class="bi bi-search text-primary"></span> klicken, oder im gesamten Text nach Wörtern suchen, indem Sie auf <span class="d-inline-block position-relative pe-3"><span class="bi bi-book text-primary"></span><i class="bi bi-search page-navigation-bar__full-search-icon text-primary"></i></span> klicken. Außerdem können Sie durch das Auswählen von <span class="text-primary">W</span> entscheiden, ob nach ganzen Wörtern oder Teilen davon gesucht werden soll. Wenn <span class="text-primary">W</span> ausgewählt ist, wird nach ganzen Wörtern gesucht, ansonsten werden alle Suchergebnisse angezeigt, in denen die gesuchte Buchstabenkombination vorkommt.</td>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function">
                                        <div class="input-group input-group-sm">
                                            <button type="button" class="btn btn-outline-primary">
                                                <span class="bi bi-chevron-left"></span>
                                            </button>
                                            <select name="page" id="file-select-chapter" class="form-select" style="width: 50%;">
                                                <option value="1" class="file-navigation-row file-navigation-link">Tabula</option>
                                                <option value="2" class="file-navigation-row file-navigation-link">Die Frauenehre</option>
                                                <option value="3" class="file-navigation-row file-navigation-link">Mauritius von Craûn</option>
                                                <option value="4" class="file-navigation-row file-navigation-link">Iwein</option>
                                                <option value="5" class="file-navigation-row file-navigation-link">Die Klage</option>
                                                <option value="6" class="file-navigation-row file-navigation-link">Das Büchlein</option>
                                                <option value="7" class="file-navigation-row file-navigation-link">Der Mantel</option>
                                                <option value="8" class="file-navigation-row file-navigation-link">Erec</option>
                                                <option value="9" class="file-navigation-row file-navigation-link">Dietrichs Flucht</option>
                                                <option value="10" class="file-navigation-row file-navigation-link">Rabenschlacht</option>
                                                <option value="11" class="file-navigation-row file-navigation-link">Nibelungenlied</option>
                                                <option value="12" class="file-navigation-row file-navigation-link">Nibelungenklage</option>
                                                <option value="13" class="file-navigation-row file-navigation-link">Kudrun</option>
                                                <option value="14" class="file-navigation-row file-navigation-link">Biterolf und Dietleib</option>
                                                <option value="15" class="file-navigation-row file-navigation-link">Ortnit</option>
                                                <option value="16" class="file-navigation-row file-navigation-link">Wolfdietrich A</option>
                                                <option value="17" class="file-navigation-row file-navigation-link">Die böse Frau</option>
                                                <option value="18" class="file-navigation-row file-navigation-link">Die treue Gattin</option>
                                                <option value="19" class="file-navigation-row file-navigation-link">Der betrogene Gatte</option>
                                                <option value="20" class="file-navigation-row file-navigation-link">Der nackte Kaiser</option>
                                                <option value="21" class="file-navigation-row file-navigation-link">Die Katze</option>
                                                <option value="22" class="file-navigation-row file-navigation-link">Frauenbuch</option>
                                                <option value="23" class="file-navigation-row file-navigation-link">Helmbrecht</option>
                                                <option value="24" class="file-navigation-row file-navigation-link">Pfaffe Amis</option>
                                                <option value="25" class="file-navigation-row file-navigation-link">Titurel</option>
                                                <option value="26" class="file-navigation-row file-navigation-link">Brief des Priesterkönigs Johannes</option>
                                            </select>
                                            <button type="button" class="btn btn-outline-primary">
                                                <span class="bi bi-chevron-right"></span>
                                            </button>
                                        </div>
                                    </td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Textnavigation<br>Mit dieser Funktion können die einzelnen Abschnitte oder Kapitel eines Textes ausgewählt werden. Es kann entweder ein Abschnitt über die Auswahlliste geöffnet werden oder mit <span class="bi bi-chevron-left text-primary"></span> zum vorhergehenden Abschnitt und mit <span class="bi bi-chevron-right text-primary"></span> zum nachfolgenden Abschnitt gesprungen werden.</td>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function">
                                        <div class="input-group-sm input-group">
                                            <button type="button" class="btn btn-outline-primary">
                                                <span class="bi bi-chevron-left"></span>
                                            </button>
                                            <select name="page" id="file-select" class="form-select" style="width: 50%;">
                                                <option value="1" class="file-navigation-row file-navigation-link">fol. 1r (19)</option>
                                                <option value="2" class="file-navigation-row file-navigation-link">fol. 1v (20)</option>
                                                <option value="3" class="file-navigation-row file-navigation-link">fol. 2r (21)</option>
                                            </select>
                                            <button type="button" class="btn btn-outline-primary">
                                                <span class="bi bi-chevron-right"></span>
                                            </button>
                                        </div>
                                    </td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Seitennavigation<br>Mit dieser Funktion können Sie zwischen den einzelnen Seiten eines Abschnittes oder Kapitels navigieren. Es werden sowohl die Foliierung als auch die Nummer des Scans angezeigt. Mit <span class="bi bi-chevron-left text-primary"></span> kommen Sie zur vorhergehenden und mit <span class="bi bi-chevron-right text-primary"></span> zur nachfolgenden Seite.</td>
                                </tr>
                                </tbody>
                            </table>
                            <h2 id="left">Transkriptionsseite</h2>
                            <div class="documentation-textblock">Auf der Transkriptionsseite finden Sie den transkribierten Text, dessen Layout dem Manuskript nachempfunden ist. Transkriptions- und Manuskriptseite sind miteinander verknüpft, sodass beim Klicken auf eine Zeile in der Transkriptionsseite die entsprechende Zeile in der Manuskriptseite hervorgehoben wird. Zudem wird nur eine Spalte dargestellt, nachdem eine Zeile ausgewählt wurde, sofern das Manuskript mehrere Spalten hat.</div>
                            <table class="documentation-table">
                                <tbody>
                                <tr class="documentation-tablerow">
                                    <th class="documentation-tablecol documentation-tablecol-header">Funktion</th>
                                    <th class="documentation-tablecol documentation-tablecol-header">Erklärung</th>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function">
                                        <span class="junicode">mein fraw</span>
                                    </td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Durch Klicken auf eine Zeile wird die entsprechende Zeile sowohl auf der Transkriptionsseite als auch auf der Manuskriptseite hervorgehoben. Zudem wird in die Spaltenansicht gewechselt, wenn der Text mehrere Spalten hat.</td>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function">
                    <span class="d-inline-block">
                        <div class="input-group input-group-sm">
                            <button id="move-left" class="btn btn-primary">
                                <span class="bi bi-arrow-left"></span>
                            </button>
                            <button id="reset-view" class="btn btn-primary mx-1">
                                <span class="bi bi-list"></span>
                            </button>
                            <button id="move-right" class="btn btn-primary">
                                <span class="bi bi-arrow-right"></span>
                            </button>
                        </div>
                    </span>
                                    </td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Spaltennavigation<br>Mit dieser Funktion können Sie durch die Spalten der Seite navigieren. Klicken Sie auf <span class="bi bi-list text-primary"></span>, um eine Spalte vergrößert oder alle Spalten zusammen anzuzeigen. Mit <span class="bi bi-arrow-left text-primary"></span> und <span class="bi bi-arrow-right text-primary"></span> können Sie zwischen den Spalten wechseln.</td>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function">
                    <span class="d-inline-block">
                        <div class="input-group input-group-sm">
                            <button id="scale-font-plus" class="btn btn-primary me-1">
                                <span class="bi bi-plus"></span>
                            </button>
                            <button id="scale-font-minus" class="btn btn-primary">
                                <span class="bi bi-dash"></span>
                            </button>
                        </div>
                    </span>
                                    </td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Text-Zoom<br>Wenn Sie auf <span class="bi bi-plus text-primary"></span> oder <span class="bi bi-dash text-primary"></span> klicken, wird der transkribierte Text vergrößert oder verkleinert. Diese Funktion ist nur möglich, wenn alle Spalten angezeigt werden.</td>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function">
                                        <button id="text-options" class="btn btn-sm btn-primary">
                                            <span class="bi bi-gear"></span>
                                        </button>
                                    </td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Optionen<br>Mit einem Klick auf <span class="bi bi-gear text-primary"></span> wird das Optionsmenü geöffnet. Bei einem weiteren Klick wird es wieder geschlossen.</td>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function"><span class="bi bi-square"></span> Versumbruch</td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Wird im Optionsmenü <span class="bi bi-gear text-primary"></span> „Versumbruch“ ausgewählt, wird der Text nach Versen und gegebenenfalls Strophen geordnet. Beim Anklicken eines Verses in der Transkriptionsseite werden alle Zeilen in der Manuskriptseite hervorgehoben, in welchen der ausgewählte Vers vorkommt.</td>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function"><span class="bi bi-square"></span> Diplomatische Transkription</td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Wird im Optionsmenü <span class="bi bi-gear text-primary"></span> „Diplomatische Transkription“ ausgewählt, werden die allographischen Buchstabenvarianten vereinheitlicht und alle Abbreviaturen aufgelöst.</td>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function"><span class="bi bi-square"></span> Abbreviaturen auflösen</td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Wird im Optionsmenü <span class="bi bi-gear text-primary"></span> „Abbreviaturen auflösen“ ausgewählt, werden alle Abkürzungen aufgelöst.</td>
                                </tr>
                                </tbody>
                            </table>
                            <h2 id="right">Manuskriptseite</h2>
                            <div class="documentation-textblock">Auf der Manuskriptseite finden Sie den Scan des transkribierten Textes, der auf der Transkriptionsseite angezeigt wird. Sie können im Bild mit dem Cursor zoomen und das Bild verschieben. Bei einem Klick auf eine Zeile wird die entsprechende Zeile sowohl auf der Manuskriptseite als auch auf der Transkriptionsseite hervorgehoben. Die Umrandung der Zeile bleibt für fünf Sekunden und verschwindet danach wieder, wenn sich der Cursor außerhalb der Manuskriptseite befindet. Sobald sich der Cursor wieder über dem Bild befindet, erscheint die Umrandung.</div>
                            <table class="documentation-table">
                                <tbody>
                                <tr class="documentation-tablerow">
                                    <th class="documentation-tablecol documentation-tablecol-header">Funktion</th>
                                    <th class="documentation-tablecol documentation-tablecol-header">Erklärung</th>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function">
                                        <button class="btn btn-primary btn-sm">
                                            <span class="bi bi-fullscreen"></span>
                                        </button>
                                    </td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Zoom zurücksetzen<br>Wenn Sie auf <span class="bi bi-fullscreen text-primary"></span> klicken, wird die Manuskriptseite in voller Bildgröße dargestellt.</td>
                                </tr>
                                <tr class="documentation-tablerow">
                                    <td class="documentation-tablecol documentation-tablecol-function">
                                        <button class="btn btn-primary btn-sm">
                                            <span class="bi bi-arrows-collapse"></span>
                                        </button>
                                    </td>
                                    <td class="documentation-tablecol documentation-tablecol-explanation">Ausgewählte Zeile fokussieren<br>Wenn Sie auf <span class="bi bi-arrows-collapse text-primary"></span> klicken, wird die ausgewählte Zeile sowohl in der Manuskriptseite als auch in der Transkriptionsseite fokussiert, wie wenn auf eine Zeile geklickt wird.</td>
                                </tr>
                                </tbody>
                            </table>
                            <h2 id="search">Suchergebnisse</h2>
                            <div class="documentation-textblock">Wenn nach einem Wort im ganzen Text gesucht wird, werden die Suchergebnisse sortiert nach Text sowie Strophe/Vers aufgelistet. Dabei wird unterschieden, ob in der allographischen oder in der diplomatischen Transkription gesucht werden soll und ob nach ganzen Wörtern oder Teilen davon gesucht werden soll. Wenn man auf einen Treffer klickt, wird die entsprechende Seite geöffnet und der Vers hervorgehoben, in der sich das gesuchte Wort befindet.<br /><br />Auf der Seite mit den Suchergebnissen kann außerdem nach weiteren Wörtern gesucht werden und ausgewählt werden, ob in der allographischen oder in der diplomatischen Transkription gesucht werden soll und ob nach ganzen Wörtern oder Teilen davon gesucht werden soll. Die Suchergebnisse lassen sich zudem als CSV-Datei exportieren.</div>
                            <div class="index-project-button-wrap">
                                <a href="https://transcriptiones.com" class="btn btn-outline-primary"><span class="bi bi-arrow-left"></span> Zur Startseite</a>
                                <a href="https://transcriptiones.com/home" class="btn btn-primary">Zu den Texten <span class="bi bi-arrow-right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot:content>
</x-layouts.app-layout>
