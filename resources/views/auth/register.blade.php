<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                    <x-essentials.card>
                        <x-slot name="header">
                            Registrieren
                        </x-slot>

                        <x-slot name="body">
                            <form method="POST" action="{{ route('register.post') }}" autocomplete="off" id="registerForm">
                                @csrf
                            </form>

                            <div>
                                <x-essentials.input-field
                                    label="E-Mail"
                                    placeholder="mustermann@gmail.com"
                                    name="email"
                                    form="registerForm"
                                    value="{{ old('email') }}"
                                    type="text"
                                    required
                                    class="input-max-size"
                                ></x-essentials.input-field>
                            </div>

                            <div class="mt-3">
                                <x-essentials.input-field
                                    label="Passwort"
                                    placeholder="Passwort"
                                    name="password"
                                    form="registerForm"
                                    type="password"
                                    class="input-max-size"
                                    required
                                ></x-essentials.input-field>
                            </div>

                            <div class="mt-3">
                                <x-essentials.input-field
                                    label="Passwort bestätigen"
                                    placeholder="Passwort"
                                    name="password_confirmation"
                                    form="registerForm"
                                    type="password"
                                    class="input-max-size"
                                    required
                                ></x-essentials.input-field>
                            </div>

                            <div class="mt-4">
                                <button type="submit" form="registerForm" class="btn btn-primary input-max-size">
                                    Registrieren
                                </button>
                            </div>
                        </x-slot>
                    </x-essentials.card>
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
