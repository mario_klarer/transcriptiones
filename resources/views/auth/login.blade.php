<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                    <x-essentials.card>
                        <x-slot name="header">
                            Bitte anmelden
                        </x-slot>

                        <x-slot name="body">
                            <form method="POST" action="{{ route('login.post') }}" autocomplete="off" id="loginForm">
                                @csrf
                            </form>

                            <div>
                                <x-essentials.input-field
                                    label="E-Mail"
                                    placeholder="mustermann@gmail.com"
                                    name="email"
                                    form="loginForm"
                                    value="{{ old('email') }}"
                                    type="text"
                                    required
                                    class="input-max-size"
                                ></x-essentials.input-field>
                            </div>

                            <div class="mt-3">
                                <x-essentials.input-field
                                    label="Passwort"
                                    placeholder="Passwort"
                                    name="password"
                                    form="loginForm"
                                    type="password"
                                    class="input-max-size"
                                    required
                                ></x-essentials.input-field>
                            </div>

                            <div class="mt-4">
                                <a class="link-primary" href="{{ route('inquire-reset') }}">
                                    Passwort vergessen?
                                </a>
                            </div>

                            <div class="mt-1">
                                <a class="link-primary" href="{{ route('register') }}">
                                    Noch keinen Account?
                                </a>
                            </div>

                            <div class="mt-1">
                                <button type="submit" form="loginForm" class="btn btn-primary input-max-size">
                                    Anmelden
                                </button>
                            </div>
                        </x-slot>
                    </x-essentials.card>
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
