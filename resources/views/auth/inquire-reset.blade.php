<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
                    <x-essentials.card>
                        <x-slot name="header">
                            Passwort zurücksetzen
                        </x-slot>

                        <x-slot name="body">
                            <form method="POST" action="{{ route('inquire-reset.post') }}" autocomplete="off" id="resetForm">
                                @csrf
                            </form>

                            <div>
                                <x-essentials.input-field
                                    label="E-Mail"
                                    placeholder="mustermann@gmail.com"
                                    name="email"
                                    form="resetForm"
                                    value="{{ old('email') }}"
                                    type="text"
                                    required
                                    class="input-max-size"
                                ></x-essentials.input-field>
                            </div>

                            <div class="mt-4">
                                <button type="submit" form="resetForm" class="btn btn-primary input-max-size">
                                    Passwort zurücksetzen
                                </button>
                            </div>
                        </x-slot>
                    </x-essentials.card>
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
