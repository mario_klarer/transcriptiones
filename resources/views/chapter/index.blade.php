<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <x-essentials.card>
                        <x-slot name="header">
                            Ambraser Heldenbuch
                        </x-slot>

                        <x-slot name="body">
                            <div class="row">
                                <div class="col-12">
                                    <h1>Forschungsprojekt</h1>
                                    <p>Das ÖAW-go!digital-2.0-Projekt <a href="https://www.uibk.ac.at/projects/ahb/" target="_blank" rel="noopener noreferrer">»Ambraser Heldenbuch: Transkription und wissenschaftliches Datenset«</a> setzte sich zum Ziel, bis zum Jahr 2019&nbsp;– dem 500.&nbsp;Todestag von Maximilian&nbsp;I.&nbsp;– das ›Ambraser Heldenbuch‹ (Wien, Österreichische Nationalbibliothek, Cod. ser. nova 2663) aus dem frühen 16.&nbsp;Jahrhundert zur Gänze zu transkribieren. Das ›Ambraser Heldenbuch‹ wurde am Beginn des 16.&nbsp;Jahrhunderts von Maximilian&nbsp;I. als Prunkhandschrift in Auftrag gegeben und vom Bozner Zöllner Hans Ried in einer Hand auf knapp 250&nbsp;großformatigen Pergamentblättern ausgeführt. In diesem Prachtkodex finden sich 25 der wichtigsten mittelhochdeutschen literarischen Texte (z.&nbsp;B. ›Nibelungenlied‹ und ›Helmbrecht‹), wovon 15 als Unikate (z.&nbsp;B. ›Erec‹ und ›Mauritius von Craûn‹) ausschließlich im ›Ambraser Heldenbuch‹ überliefert sind.</p>
                                    <p>Aufgrund der großen Textmenge in einer Schreiberhand&nbsp;– das ›Ambraser Heldenbuch‹ und vier weitere bekannte Schriftzeugnisse (Urkunde, Zollregister und Dienstreversen) von Hans Ried umfassen ca.&nbsp;600.000&nbsp;Wörter&nbsp;– eignet sich eine Gesamttranskription der Ried’schen Autographe als einmaliges linguistisches und literaturwissenschaftliches Korpus:</p>
                                    <ol>
                                        <li>Aus editionsphilologischer Sicht ermöglicht das Korpus einzigartige Möglichkeiten für die mittelhochdeutsche Textrekonstruktion der als Unikate im ›Ambraser Heldenbuch‹ überlieferten Werke, aber auch für die Edition und Erforschung der Parallelüberlieferungen (z.&nbsp;B. ›Nibelungenlied‹).</li>
                                        <li>Aus linguistischer und dialektologischer Sicht erschließt die Transkription des ›Ambraser Heldenbuchs‹ zusammen mit den anderen Schriftstücken von Hans Ried ein geographisch liminales Großkorpus am Übergang von Mittelalter zu Früher Neuzeit aus einer Schreiberhand.</li>
                                    </ol>
                                    <p class="mb-0">Unter historischen Linguist*innen und Editionsphilolog*innen gilt die Gesamttranskription des ›Ambraser Heldenbuchs‹ und der Ried’schen Autographe seit vielen Jahren als vorrangiges Forschungsdesiderat. Ziel des Projekts war daher eine auf dem Stand der Technik durchgeführte elaborierte Transkription des Gesamttexts des ›Ambraser Heldenbuchs‹ sowie die Aufbereitung dieser Transkription als öffentlich zugängliches und zitierbares Datenset, das als künftiges Referenzdokument für eine breite Palette an wissenschaftlichen Arbeiten dienen soll.</p>
                                </div>
                            </div>

                            <hr class="my-4" />

                            <div class="row">
                                <h3>Text wählen</h3>

                                @foreach($chapters as $chapter)
                                    <a href="{{ route('page.show', ['page' => $chapter->first_page->id]) }}" class="col-12 col-md-6 col-lg-4 col-xl-3 my-3">
                                        <x-essentials.card class="h-100" img-link="{{ $chapter->teaser_image_link }}">
                                            <x-slot name="body">
                                                <h5>
                                                    {{ $chapter->name }}
                                                </h5>

                                                @foreach($chapter->first_three_lines as $line)
                                                    <div class="junicode">
                                                        {{ $line->value }}
                                                    </div>
                                                @endforeach
                                            </x-slot>
                                        </x-essentials.card>
                                    </a>
                                @endforeach
                            </div>
                        </x-slot>
                    </x-essentials.card>
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
