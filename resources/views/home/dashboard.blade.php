<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <x-essentials.card>
                        <x-slot name="header">
                            Text wählen
                        </x-slot>

                        <x-slot name="body">
                            <div class="row">
                                <a href="{{ route('chapter') }}" class="col-12 col-md-6 col-lg-4 col-xl-3">
                                    <x-essentials.card class="h-100" img-link="{{ safeMix('/assets/pages/00000018.jpg') }}">
                                        <x-slot name="body">
                                            <h5 class="mb-0">
                                                Ambraser Heldenbuch
                                            </h5>
                                        </x-slot>
                                    </x-essentials.card>
                                </a>
                            </div>
                        </x-slot>
                    </x-essentials.card>
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
