<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-8 offset-xl-2">
                    <x-essentials.card>
                        <x-slot name="body">
                            @if($search !== '')
                                <h2>Suchergebnisse für „{{ $search }}“ ({{ $totalCount }} Treffer)</h2>
                            @else
                                <h2>Suche</h2>
                            @endif

                            <form method="GET" action="{{ route('search') }}" autocomplete="off" id="searchForm">
                            </form>

                            <div>
                                <x-essentials.input-field
                                    label="Suchterm"
                                    placeholder="Suchterm"
                                    name="search"
                                    form="searchForm"
                                    value="{{ $search }}"
                                    type="text"
                                    required
                                    class="input-max-size"
                                ></x-essentials.input-field>
                            </div>

                            <div class="mt-1">
                                <div class="form-check">
                                    <input @if($fullWord) checked @endif form="searchForm" class="form-check-input" type="checkbox" name="full_word" value="true" id="fullWordCheck">
                                    <label class="form-check-label" for="fullWordCheck">
                                        Suche nach ganzen Wörtern
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input @if($normalized) checked @endif form="searchForm" class="form-check-input" type="checkbox" name="normalized" value="true" id="normalizedCheck">
                                    <label class="form-check-label" for="normalizedCheck">
                                        Suche in diplomatischer Transkription
                                    </label>
                                </div>
                            </div>

                            <div class="mt-3">
                                <button type="submit" form="searchForm" class="btn btn-primary input-max-size">
                                    Suchen
                                </button>
                            </div>

                            @if($lines->count() > 0)
                                <div class="mt-3">
                                    <a href="{{ route('search', ['search' => $search, 'normalized' => $normalized ? 'true' : 'false', 'full_word' => $fullWord ? 'true' : 'false', 'export' => 'true']) }}" class="btn btn-primary input-max-size">
                                        Suchergebnisse exportieren
                                    </a>
                                </div>
                            @endif
                        </x-slot>
                    </x-essentials.card>

                    <x-essentials.card class="mt-3">
                        <x-slot name="body">
                            <div class="row text-center py-2">
                                <div class="col-6">
                                    <strong>
                                        Text
                                    </strong>
                                </div>
                                <div class="col-6">
                                    <strong>
                                        Strophe/Vers
                                    </strong>
                                </div>
                            </div>
                            @foreach($lines as $line)
                                <a href="{{ route('page.show', ['page' => $line->page->id, 'search' => $search, 'verse' => $line->verse_id, 'full_word' => $fullWord, 'normalized' => $normalized]) }}" class="row text-center py-1">
                                    <div class="col-6">
                                        {{ $line->page->chapter->name }}
                                    </div>
                                    <div class="col-6">
                                        {!! $line->strophe_number !== null ? ($line->strophe_number . ',' . $line->verse_number) : $line->verse_number  !!}
                                    </div>
                                </a>
                            @endforeach
                            <div class="mt-3">
                                {{ $lines->appends(request()->query())->links() }}
                            </div>
                        </x-slot>
                    </x-essentials.card>
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
