<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<meta name="robots" content="noindex">
<meta name="description" content="{{ $description }}">

<meta property="og:url"                content="{{ route('index') }}" />
<meta property="og:type"               content="website" />
<meta property="og:title"              content="Transcriptiones.com" />
<meta property="og:description"        content="{{ $description }}" />
<meta property="og:image"              content="" />

<link rel="icon" href="favicon.ico">

<title>{{ $title }}</title>
