<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 py-3 py-lg-1 col-lg-3">
                <a href="{{ route('index') }}" class="link-primary">
                    Home
                </a>
            </div>
            <div class="col-12 py-3 py-lg-1 col-lg-3">
                <a href="{{ route('imprint') }}" class="link-primary">
                    Impressum
                </a>
            </div>
            <div class="col-12 py-3 py-lg-1 col-lg-3">
                <a href="{{ route('dataprotection') }}" class="link-primary">
                    Datenschutz
                </a>
            </div>
            <div class="col-12 py-3 py-lg-1 col-lg-3">
                <a href="{{ route('terms') }}" class="link-primary">
                    Nutzungsbedingungen
                </a>
            </div>
        </div>
    </div>
</div>
