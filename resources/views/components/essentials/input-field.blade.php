<div>
    @isset($label)
        <div class="text-small">
            <strong>
                {{ $label }}
            </strong>
        </div>
    @endif

    <input
        {{ $attributes->merge(['class' => 'form-control']) }}
    />
</div>
