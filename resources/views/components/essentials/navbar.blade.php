<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('index') }}">
            <b>Transcriptiones</b>.com
        </a>
        <div class="d-flex ms-auto">
            @auth
                <a href="{{ route('logout') }}" class="d-flex px-2 py-1 border border-1 border-secondary rounded-circle align-items-center justify-content-center">
                    <i class="bi bi-box-arrow-right"></i>
                </a>
            @endauth
        </div>
    </div>
</nav>
