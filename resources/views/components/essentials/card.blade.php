<div {{ $attributes->merge(['class' => 'card']) }}>
    @isset($header)
        <div class="card-header">
            {{ $header }}
        </div>
    @endisset
    @isset($imgLink)
        <img class="card-img-top" src="{{ $imgLink }}">
    @endisset
    <div class="card-body">
        {{ $body }}
    </div>
    @isset($footer)
        <div class="card-footer">
            {{ $footer }}
        </div>
    @endisset
</div>
