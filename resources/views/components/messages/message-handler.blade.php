<vue-message-handler
    error-key="{{ config('app.error_key') }}"
    success-key="{{ config('app.success_key') }}"
    :messages="{{ $messages }}"
></vue-message-handler>
