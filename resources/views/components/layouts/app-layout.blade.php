<x-layouts.base-layout>
    @if(isset($head))
        <x-slot name="head">
            {{ $head }}
        </x-slot>
    @endif

    @if(isset($styles))
        <x-slot:styles>
            {{ $styles }}
        </x-slot:styles>
    @endif

    <x-slot name="body">
        <div id="app">
            <x-messages.message-handler
                :errors="session(config('app.error_key'))"
                :success="session(config('app.success_key'))"
            ></x-messages.message-handler>

            @if(isset($navbar))
                {{ $navbar }}
            @else
                <x-essentials.navbar />
            @endif

            <div class="app">
                {{ $content }}
            </div>

            @if(isset($sidebar))
                {{ $sidebar }}
            @else
                <x-essentials.sidebar />
            @endif

            @if(isset($footer))
                {{ $footer }}
            @else
                <x-essentials.footer />
            @endif
        </div>
    </x-slot>

    @if(isset($scripts))
        <x-slot name="scripts">
            {{ $scripts }}
        </x-slot>
    @endif
</x-layouts.base-layout>
