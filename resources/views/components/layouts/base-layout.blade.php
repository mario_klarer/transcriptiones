<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @if(isset($head))
        {{ $head }}
    @else
        <x-essentials.head/>
    @endif

    @if(isset($styles))
        {{ $styles }}
    @else
        <x-essentials.style />
    @endif
</head>

<body>
{{ $body }}

@if(isset($scripts))
    {{ $scripts }}
@else
    <x-essentials.javascript />
@endif
</body>
</html>
