<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center index-container">
                <div class="col-12 col-xl-8">
                    <x-essentials.card>
                        <x-slot name="body">
                            <div class="text-center">
                                <h1>Transcriptiones</h1>

                                <p>Transkribierte Texte und ihre Scans interaktiv erkunden</p>

                                <a href="{{ route('dashboard') }}" class="btn btn-primary">Transkriptionen</a>

                                <a href="{{ route('explanation') }}" class="btn btn-primary">Funktionsweise</a>

                                <a href="https://gitlab.com/mario_klarer/transcriptiones" class="btn btn-primary">Quellcode</a>
                            </div>
                        </x-slot>
                    </x-essentials.card>
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
