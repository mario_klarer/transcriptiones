<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-8 offset-xl-2">
                    <x-essentials.card>
                        <x-slot name="body">
                            <h1>Nutzungsbedingungen</h1>
                            <p>Stand: 5. Mai 2023</p>
                            <h2>I. Vorwort</h2>
                            <p>Hallo und herzlich willkommen bei Transcriptiones.com. Wir (im Folgenden wird Transcriptiones.com auch „wir“, „uns“ und „unser/e/er/es“ genannt) bieten eine Internetplattform (<a href="https://transcriptiones.com" target="_blank" rel="noopener noreferrer">transcriptiones.com</a>) an, welche Ihnen, der*m Nutzer*in, kostenlose Möglichkeiten bieten, transkribierte Texte und ihre Scans interaktiv zu erkunden. Der Zugriff auf unsere Webseite und die Nutzung dieser – im Folgenden „Dienste“ genannt – unterliegt unseren in diesem Dokument enthaltenen Nutzungsbedingungen und der Datenschutzerklärung (abrufbar unter <a href="https://www.transcriptiones.com/data-protection" target="_blank" rel="noopener noreferrer">Datenschutz</a>).</p>
                            <p>Bitte nutzen Sie unsere Dienste nur, wenn Sie mit den Richtlinien einverstanden sind und diese auch befolgen und akzeptieren. Anderenfalls dürfen Sie unsere Dienste nicht in Anspruch nehmen!</p>
                            <h2>II. Nutzungsbedingungen</h2>
                            <p>1. Die Dienste dürfen ausschließlich von der*m jeweiligen Nutzer*in in Anspruch genommen werden. Sie sind nicht übertragbar.</p>
                            <p>2. Sämtliche auf Transcriptiones.com befindlichen Daten, angezeigten Inhalte und Informationen dürfen nicht an Dritte weitergegeben werden, ausgenommen dies erfolgt unter den in Punkt 4 angeführten Bedingungen.</p>
                            <p>3. Transcriptiones.com haftet nicht auf Richtigkeit und Vollständigkeit der Informationen dieser Webseite. Jede*r Nutzer*in trägt selbst dafür Verantwortung, in welcher Form sie*er die von Transcriptiones.com zur Verfügung gestellten Daten nützt.</p>
                            <p>4. Die auf Transcriptiones.com enthaltenen Daten und Inhalte sind urheberrechtlich geschützt. Bei korrekter Wiedergabe und richtiger Quellenangabe „Transcriptiones.com“ ist es erlaubt, die Daten und Inhalte zu verbreiten und öffentlich zugänglich zu machen, solange es sich um keine der urheberrechtlich geschützten Daten handelt.</p>
                            <p>Sie sind sich im Klaren, dass bei der Verletzung der in der „Nutzungsbedingungen“ angeführten Richtlinien, Sie uns das Recht einräumen, Ihren Zugriff unangekündigt und permanent zu sperren.</p>
                            <p>Wir werden diese Nutzungsbedingungen von Zeit zu Zeit aktualisieren. Wir informieren über Änderungen auf unserer Webseite.</p>
                        </x-slot>
                    </x-essentials.card>
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
