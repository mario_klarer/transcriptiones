<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-8 offset-xl-2">
                    {{ $test }}
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
