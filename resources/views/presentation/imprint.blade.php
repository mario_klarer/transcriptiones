<x-layouts.app-layout>
    <x-slot name="content">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-8 offset-xl-2">
                    <x-essentials.card>
                        <x-slot name="body">
                            <h1>Impressum</h1>
                            <p>Stand: 5. Mai 2023</p>
                            <p>Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63 Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz.</p>
                            <p>Mario Klarer<br>Wetterkreuzweg 8b<br>6170 Zirl<br>Österreich</p>
                            <p>Tel.: +436643944121<br>E-Mail: klarer.presentations@gmail.com</p>
                            <p>Quelle: Erstellt mit dem <a href="https://www.firmenwebseiten.at/impressum-generator/" target="_blank" rel="noopener noreferrer">Impressum Generator von firmenwebseiten.at</a> in Kooperation mit <a href="https://www.dasedle.at" target="_blank" rel="noopener noreferrer">dasedle.at</a></p>
                            <h2>EU-Streitschlichtung</h2>
                            <p>Angaben zur Online-Streitbeilegung: Verbraucher haben die Möglichkeit, Beschwerden an die Online-Streitbeilegungsplattform der EU zu richten: <a href="https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&amp;lng=DE" target="_blank" rel="noopener noreferrer">https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&amp;lng=DE</a>. Sie können allfällige Beschwerden auch an die oben angegebene E-Mail-Adresse richten.</p>
                            <h2>Haftung für Inhalte dieser Webseite</h2>
                            <p>Wir entwickeln die Inhalte dieser Webseite ständig weiter und bemühen uns, korrekte und aktuelle Informationen bereitzustellen. Leider können wir keine Haftung für die Korrektheit aller Inhalte auf dieser Webseite übernehmen, speziell für jene die seitens Dritter bereitgestellt wird. Sollten Ihnen problematische oder rechtswidrige Inhalte auffallen, bitten wir Sie, uns zu kontaktieren. Sie finden die Kontaktdaten im Impressum.</p>
                            <h2>Haftung für Links auf dieser Webseite</h2>
                            <p>Unsere Webseite enthält Links zu anderen Webseiten, für deren Inhalt wir nicht verantwortlich sind. Wenn Ihnen rechtswidrige Links auf unserer Webseite auffallen, bitten wir Sie, uns zu kontaktieren. Sie finden die Kontaktdaten im Impressum.</p>
                            <h2>Urheberrechtshinweis</h2>
                            <p>Alle Inhalte dieser Webseite unterliegen dem Urheberrecht. Falls notwendig, werden wir die unerlaubte Nutzung von Teilen der Inhalte unserer Seite rechtlich verfolgen.</p>
                        </x-slot>
                    </x-essentials.card>
                </div>
            </div>
        </div>
    </x-slot>
</x-layouts.app-layout>
