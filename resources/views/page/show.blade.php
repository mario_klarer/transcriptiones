<x-layouts.app-layout>
    <x-slot name="navbar">
        <vue-page-navigation-bar dashboard-link='{{ route('index') }}'
                                 search-link='{{ route('search') }}'
                                 chapter-name='{{ $currentChapter->name }}'
                                 :all-pages='@json($allPagesOfChapter)'
                                 :all-chapters='@json($allChapters)'
                                 :currently-viewed-page='@json($page)'
                                 :currently-viewed-chapter='@json($currentChapter)'
                                 search-value='{{ $search }}'
                                 :is-full-word-search-enabled='{{ $fullWord ? 'true' : 'false' }}'
                                 :is-normalized-search-enabled='{{ $normalized ? 'true' : 'false' }}'
        ></vue-page-navigation-bar>
    </x-slot>

    <x-slot name="content">
        <vue-page-viewer :currently-viewed-page='@json($page)' :currently-viewed-chapter='@json($currentChapter)' :preselected-verse="{{ $preselectedVerse ?? 'null' }}" :show-normalized="{{ $normalized ? 'true' : 'false' }}">
        </vue-page-viewer>
    </x-slot>
</x-layouts.app-layout>
