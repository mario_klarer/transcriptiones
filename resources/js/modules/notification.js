import Vue from 'vue'

/**
 * The Notification represents a global Vue object, allowing to trigger notifications.
 */

const Notification = new Vue({
    methods: {
        showWarning(msg, timeout = 10000) {
            this.showNoty(msg, 'warning', timeout);
        },
        showError(msg, timeout = 10000) {
            this.showNoty(msg, 'error', timeout);
        },
        showSuccess(msg, timeout = 10000) {
            this.showNoty(msg, 'success', timeout);
        },
        showInfo(msg, timeout = 10000) {
            this.showNoty(msg, 'info', timeout);
        },
        showNoty(msg, type, timeout) {
            if(msg) {
                new Noty(
                    {
                        text: msg,
                        type: type,
                        theme: 'relax',
                        timeout: timeout,
                    },
                ).show();
            }
        },
    }
});

export { Notification };
