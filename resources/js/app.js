require('./bootstrap');

Vue.component('vue-message-handler', require('Components/messages/MessageHandler.vue').default);
Vue.component('vue-page-navigation-bar', require('Components/navigation/PageNavigationBar.vue').default);
Vue.component('vue-page-viewer', require('Components/page/PageViewer.vue').default);

