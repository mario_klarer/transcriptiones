window._ = require('lodash');

try {
    require('bootstrap');
} catch (e) {}

import $ from 'jquery';
window.$ = window.jQuery = $;

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Accept'] = 'application/json';
window.axios.defaults.headers.common['Content-Type'] = 'application/json';

window.Noty = require('noty');

window.Vue = require('vue').default;

import { Notification } from 'Modules/notification.js';

Vue.prototype.$Notifications = Notification;
Vue.prototype.$EventBus = new Vue();

import VTooltip from 'v-tooltip';

Vue.use(VTooltip);
