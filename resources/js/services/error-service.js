class ErrorService {
    parseErrorMessage(error) {
        if (error.response) {
            let message = error.response.data.message;

            if (error.response.status === 422
                && error.response.data.errors !== undefined
                && Object.keys(error.response.data.errors).length > 0) {
                // message = Lang.get('error_codes.javascript.422') + " <br />";
                message = "Bitte beachte Folgendes: <br />";

                let amountMessages = 0;

                for (let key of Object.keys(error.response.data.errors)) {
                    let currentError = error.response.data.errors[key];
                    for (let errorMessage of currentError) {
                        message += "<li class='my-2'>" + errorMessage + "</li>"
                        amountMessages++;
                    }

                    if (amountMessages > 5)
                        return message;
                }
            } else if (error.response.status === 401) {
                // message = Lang.get('error_codes.javascript.401');
                message = 'Anscheinend bist du nicht angemeldet. Melde dich an und versuch es bitte nochmal.';
            } else if (error.response.status === 403) {
                // message = Lang.get('error_codes.javascript.403');
                message = 'Du hast keinen Zugriff auf diese Seite.';
            } else if (error.response.status === 500) {
                // message = Lang.get('error_codes.javascript.500');
                message = 'Etwas ist schief gelaufen. Wenn dieses Problem weiterhin besteht, wende dich bitte an unseren Support.';
            }

            return message;
        } else if (error.request) {
            if (error.request.status === 422) {
                if (error.data.message) {
                    return error.data.message;
                }
            }
            // return Lang.get('error_codes.javascript.timeout');
            return 'Wir können unsere Server nicht erreichen. Bitte stellen sicher, dass du mit dem Internet verbunden bist.';
        } else {
            // return Lang.get('error_codes.javascript.500');
            return 'Etwas ist schief gelaufen. Wenn dieses Problem weiterhin besteht, wende dich bitte an unseren Support.';
        }
    }
}

export default new ErrorService();
