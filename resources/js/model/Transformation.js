class Transformation {
    constructor(name, start, end, values) {
        this.name = name;
        this.start = start;
        this.end = end;
        this.values = values;
    }

    static fromJson(json) {
        return new Transformation(json.name, json.start, json.end, json.values);
    }
}

export default Transformation;
