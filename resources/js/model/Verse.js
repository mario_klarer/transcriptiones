import Strophe from "./Strophe";
import Transformation from "./Transformation";
import Transformable from "./Transformable";

class Verse extends Transformable {
    constructor(id, readingNumber, verseNumber, value, minX, minY, maxX, maxY, indent, textAreaId, strophe, transformations, normalizedTransformations) {
        super(Verse.preprocessValue(value), transformations, normalizedTransformations);
        this.transformedNormalizedValue = this.replacePunctures(this.transformedNormalizedValue);
        this.id = id;
        this.verseNumber = verseNumber;
        this.readingNumber = readingNumber;
        this.minX = minX;
        this.minY = minY;
        this.maxX = maxX;
        this.maxY = maxY;
        this.indent = indent;
        this.textAreaId = textAreaId;
        this.strophe = strophe;
    }

    static preprocessValue(value) {
        value = Verse.replaceContinuationsWithEmptyCharacters(value);

        return value;
    }

    static replaceContinuationsWithEmptyCharacters(value) {
        return value.replaceAll('⸗ ', '‎‎').replaceAll('⸗', '‎');
    }

    static fromJson(json) {
        let strophe = null;

        if(json.strophe !== null && json.strophe !== undefined) {
            strophe = Strophe.fromJson(json.strophe);
        }

        let transformations = [];

        if(json.transformation_options !== null && json.transformation_options !== undefined) {
            for(let option of json.transformation_options) {
                transformations.push(Transformation.fromJson(option));
            }
        }

        let normalizedTransformations = [];

        if(json.transformation_options_normalized !== null && json.transformation_options_normalized !== undefined) {
            for(let option of json.transformation_options_normalized) {
                normalizedTransformations.push(Transformation.fromJson(option));
            }
        }

        return new Verse(json.id, json.reading_number, json.verse_number, json.value, json.min_x.split(',').map((x) => parseInt(x)), json.min_y.split(',').map((x) => parseInt(x)), json.max_x.split(',').map((x) => parseInt(x)), json.max_y.split(',').map((x) => parseInt(x)), json.verse_indent, json.text_area_id, strophe, transformations, normalizedTransformations);
    }

    replacePunctures(string) {
        const toReplace = ['·', ':', '~', '', '⸗', '∧'];

        let ret = string;

        toReplace.forEach((v) => {
            ret = ret.replaceAll(' ' + v, '');
            ret = ret.replaceAll(v, '');
        })

        return ret;
    }
}

export default Verse;
