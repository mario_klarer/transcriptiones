import Line from "./Line";
import Verse from "./Verse";
import Strophe from "./Strophe";

class TextArea {
    constructor(id, readingNumber, lines, minX, minY, maxX, maxY) {
        this.id = id;
        this.readingNumber = readingNumber;
        this.lines = lines;
        this.minX = minX;
        this.minY = minY;
        this.maxX = maxX;
        this.maxY = maxY;
    }

    static fromJson(json) {
        let lines = [];
        if(json.lines !== null && json.lines !== undefined) {
            for(let line of json.lines) {
                lines.push(Line.fromJson(line));
            }
        }

        return new TextArea(json.id, json.reading_number, lines, json.min_x, json.min_y, json.max_x, json.max_y);
    }

    hasNonEmptyLines() {
        for(let line of this.lines) {
            if(line.value !== null && line.value !== '')
                return true;
        }

        return false;
    }
}

export default TextArea;
