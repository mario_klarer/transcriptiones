import Transformation from "./Transformation";
import Transformable from "./Transformable";

class Line  extends Transformable {
    constructor(id, readingNumber, lineNumber, value, lineIndent, smallIndent, minX, minY, maxX, maxY, transformations, normalizedTransformations) {
        super(value, transformations, normalizedTransformations);
        this.id = id;
        this.readingNumber = readingNumber;
        this.lineNumber = lineNumber;
        this.lineIndent = lineIndent;
        this.smallIndent = smallIndent;
        this.minX = minX;
        this.minY = minY;
        this.maxX = maxX;
        this.maxY = maxY;
        this.absoluteCluster = 0;
        this.absoluteClusterIndex = 0;
        this.absoluteClusterReference = minY;
    }

    static fromJson(json) {
        let transformations = [];

        if(json.transformation_options !== null && json.transformation_options !== undefined) {
            for(let option of json.transformation_options) {
                transformations.push(Transformation.fromJson(option));
            }
        }

        let normalizedTransformations = [];

        if(json.transformation_options_normalized !== null && json.transformation_options_normalized !== undefined) {
            for(let option of json.transformation_options_normalized) {
                normalizedTransformations.push(Transformation.fromJson(option));
            }
        }

        return new Line(json.id, json.reading_number, json.line_number, json.value, json.line_indent, json.small_indent, json.min_x, json.min_y, json.max_x, json.max_y, transformations, normalizedTransformations);
    }

    static isNumber(val) {
        return /^\d+$/.test(val);
    }

    setAbsoluteClusterIndex(val) {
        this.absoluteClusterIndex = val;
    }

    setAbsoluteClusterReference(val) {
        this.absoluteClusterReference = val;
    }

    setAbsoluteCluster(val) {
        this.absoluteCluster = val;
    }
}

export default Line;
