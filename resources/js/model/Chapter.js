import Page from "./Page";

class Chapter {
    constructor(id, nextChapter, previousChapter, name, pages, link, hasStrophes, isAbsolute) {
        this.id = id;
        this.nextChapter = nextChapter;
        this.previousChapter = previousChapter;
        this.name = name;
        this.pages = pages;
        this.link = link;
        this.hasStrophes = hasStrophes;
        this.isAbsolute = isAbsolute;
    }

    static fromJson(json) {
        let pages = [];
        let nextChapter = null;
        let previousChapter = null;

        if(json.pages !== null && json.pages !== undefined) {
            for(let page of json.pages) {
                pages.push(Page.fromJson(page));
            }
        }

        if(json.next_chapter !== null && json.next_chapter !== undefined) {
            nextChapter = Chapter.fromJson(json.next_chapter)
        }

        if(json.previous_chapter !== null && json.previous_chapter !== undefined) {
            previousChapter = Chapter.fromJson(json.previous_chapter)
        }

        return new Chapter(json.id, nextChapter, previousChapter, json.name, pages, json.link, json.has_strophes == 1, json.is_absolute == 1);
    }
}

export default Chapter;
