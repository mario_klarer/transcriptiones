import TextArea from "./TextArea";
import Verse from "./Verse";
import Strophe from "./Strophe";

class Page {
    constructor(id, nextPage, previousPage, folio, pageNumber, textAreas, verses, strophes, link, imageLink, imageWidth, imageHeight) {
        this.id = id;
        this.nextPage = nextPage;
        this.previousPage = previousPage;
        this.pageNumber = pageNumber;
        this.folio = folio;
        this.textAreas = textAreas;
        this.link = link;
        this.imageLink = imageLink;
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
        this.verses = verses;
        this.strophes = strophes;
    }

    static fromJson(json) {
        let textAreas = [];
        let nextPage = null;
        let previousPage = null;

        if(json.text_areas !== null && json.text_areas !== undefined) {
            for(let textArea of json.text_areas) {
                textAreas.push(TextArea.fromJson(textArea));
            }
        }

        if(json.next_page !== null && json.next_page !== undefined) {
            nextPage = Page.fromJson(json.next_page)
        }

        if(json.previous_page !== null && json.previous_page !== undefined) {
            previousPage = Page.fromJson(json.previous_page)
        }

        let verses = [];
        if(json.verses !== null && json.verses !== undefined) {
            for(let verse of json.verses) {
                verses.push(Verse.fromJson(verse));
            }
        }

        let strophes = [];
        if(json.strophes !== null && json.strophes !== undefined) {
            for(let strophe of json.strophes) {
                strophes.push(Strophe.fromJson(strophe));
            }
        }

        return new Page(json.id, nextPage, previousPage, json.folio, json.page_number, textAreas, verses, strophes, json.link, json.image_link, json.image_width, json.image_height);
    }
}

export default Page;
