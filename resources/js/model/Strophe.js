import Verse from "./Verse";

class Strophe {
    constructor(id, readingNumber, stropheNumber, textAreaId, verses) {
        this.id = id;
        this.stropheNumber = stropheNumber;
        this.readingNumber = readingNumber;
        this.textAreaId = textAreaId;
        this.verses = verses;
    }

    hasIncipitAsFirstLine() {
        return this.verses.length > 0 && this.verses[0].isIncipit();
    }

    static fromJson(json) {
        let verses = [];

        if(json.verses !== null && json.verses !== undefined) {
            for(let verse of json.verses) {
                verses.push(Verse.fromJson(verse));
            }
        }

        return new Strophe(json.id, json.reading_number, json.strophe_number, json.text_area_id, verses);
    }
}

export default Strophe;
