class Transformable {
    constructor(value, transformations, normalizedTransformations) {
        this.value = value;
        this.normalizedValue = this.normalizeString(value);
        this.transformations = transformations;
        this.normalizedTransformations = normalizedTransformations;

        this.transformedValue = '';
        this.transformedNormalizedValue = '';

        if(this.value === null || this.normalizedValue === null) {
            return;
        }

        this.transformedValue = this.applyEnclosingTransformation(this.value, this.transformations);
        this.transformedNormalizedValue = this.normalizeString(this.applyEnclosingTransformation(this.value, this.normalizedTransformations));
    }

    isIncipit() {
        return this.transformations.filter((x) => x.name === 'incipit').length > 0;
    }

    hasAmen() {
        return this.transformations.filter((x) => x.name === 'amen').length > 0;
    }

    aventiureNumber() {
        return this.transformations.filter((x) => x.name === 'aventiure').length > 0 ? this.transformations.find((x) => x.name === 'aventiure')?.values['aventiureValue'] : null;
    }

    isSpecialAventiure() {
        return this.transformations.find((x) => x.name === 'aventiure')?.values['specialAventiure'] !== undefined;
    }

    applyEnclosingTransformation(value, transformations) {
        const enclosingOptions = {
            'incipit': {
                'start': '<span %values% class="incipit">',
                'end': '</span>',
            },
            'redInitial': {
                'start': '<span %values% class="red-initial height-%height% offset-text-%offsetText% %isThinInitial%">',
                'end': '</span>',
            },
            'blueInitial': {
                'start': '<span %values% class="blue-initial height-%height% offset-text-%offsetText% %isThinInitial%">',
                'end': '</span>',
            },
            'redRubric': {
                'start': '<span %values% class="red-rubric">',
                'end': '</span>',
            },
            'bigInitial': {
                'start': '<span %values% class="big-initial height-%height% offset-text-%offsetText% %isThinInitial%">',
                'end': '</span>',
            },
            'missingInitial': {
                'start': '<span %values% class="missing-initial">',
                'end': '</span>',
            },
            'strikethrough': {
                'start': '<span %values% class="strikethrough">',
                'end': '</span>',
            },
            'insertion': {
                'start': '<span %values% class="insertion">',
                'end': '</span><span class="insertion-insert">%insert%</span>',
            },
            'caesura': {
                'start': '<span %values% class="caesura">',
                'end': '</span>',
            },
            'abbreviation': {
                'start': '<span %values% class="abbreviation">',
                'end': '</span><span class="abbreviation-expansion">%expansion%</span>',
            },
        };

        let splitValue = value.split('');

        for(let transformation of transformations) {
            if(!Object.keys(enclosingOptions).includes(transformation.name)) {
                continue;
            }

            splitValue[transformation.start] = this.insertTransformationValues(enclosingOptions[transformation.name].start, transformation) + splitValue[transformation.start];

            // We need to apply transformations on the expansion too
            if(transformation.name === 'abbreviation' && transformation.values['expansion'] !== undefined) {
                const possibleIntersections = ['bigInitial', 'blueInitial', 'redInitial', 'redRubric'];

                let intersectingTransformations = transformations.filter(x => (possibleIntersections.includes(x.name) && x.start >= transformation.start && x.end <= transformation.end) );

                let splitExpansion = transformation.values['expansion'].split('');

                for(let intersectingTransformation of intersectingTransformations) {
                    splitExpansion[intersectingTransformation.start - transformation.start] = this.insertTransformationValues(enclosingOptions[intersectingTransformation.name].start, intersectingTransformation) + splitExpansion[intersectingTransformation.start - transformation.start];
                    splitExpansion[intersectingTransformation.end - transformation.start - 1] = splitExpansion[intersectingTransformation.end - transformation.start - 1] + this.insertTransformationValues(enclosingOptions[intersectingTransformation.name].end, intersectingTransformation);
                }

                transformation.values['expansion'] = this.removeUnicodeCodes(splitExpansion.reduce((x, y) => (x + y), ''));
            }

            splitValue[transformation.end - 1] = splitValue[transformation.end - 1] + this.insertTransformationValues(enclosingOptions[transformation.name].end, transformation);
        }

        // Replacing "height" value, if forgotten with 1
        // Replacing "isThinInitial" value if forgotten with empty string
        return splitValue.reduce((x, y) => (x + y), '').replaceAll('%height%', '1').replaceAll('%isThinInitial%', '');
    }

    insertTransformationValues(value, transformation) {
        for(let key of Object.keys(transformation.values)) {
            value = value.replaceAll('%' + key + '%', transformation.values[key]);
        }

        return value.replace('%values%', this.createTransformationValueString(transformation));
    }

    createTransformationValueString(transformation) {
        let ret = '';
        for(let key of Object.keys(transformation.values)) {
            ret += key + '="' + transformation.values[key] + '" ';
        }

        return ret;
    }

    removeUnicodeCodes(string) {
        return string.replace(/\\u[\dA-Fa-f]{4}/g, match => {
            return String.fromCharCode(parseInt(match.slice(2), 16));
        });
    }

    normalizeString(string) {
        return Transformable.normalizeString(string);
    }

    static normalizeString(string) {
        if(string === null)
            return null;


        const toReplace = [
            "∂",
            "ɧ",
            "ɱ",
            "ŋ",
            "ꝛ",
            "ɞ",
            "σ",
            "ſ",
            "ꜩ",
            "ʒ",
            "",
            "ỽ",
            "ⱳ",
            "ͦ",
            "̆",
            "ÿ",
            "Ÿ",
            "Yᷓ",
            "Ѣᷓ",
            "ä",
            "Ä",
            "ö",
            "Ö",
            "ü",
            "Ü",
            "ë",
            "Ë",
            "yᷓ",
            "yᷓ",
            "aᷓ",
            "Aᷓ",
            "eᷓ",
            "Eᷓ",
            "oᷓ",
            "Oᷓ",
            "euᷓ",
            "Euᷓ",
            "uᷓ",
            "Uᷓ",
            "vᷓ",
            "Vᷓ",
            "wᷓ",
            "Wᷓ",
            "̄",
            "ˀ",
        ];
        const replacingChars = [
            "d",
            "h",
            "m",
            "n",
            "r",
            "s",
            "s",
            "s",
            "tz",
            "z",
            "s",
            "v",
            "w",
            "o",
            "",
            "y",
            "Y",
            "Y",
            "Ѣϸ",
            "ä",
            "Ä",
            "ö",
            "Ö",
            "ü",
            "Ü",
            "e",
            "E",
            "y",
            "Y",
            "ä",
            "Ä",
            "e",
            "E",
            "ö",
            "Ö",
            "eu",
            "Eu",
            "ü",
            "Ü",
            "v̈",
            "V̈",
            "w",
            "w",
            "",
            "",
        ];

        toReplace.forEach((v, i) => {
            string = string.replaceAll(v, replacingChars[i]);
        });

        return string;
    }
}

export default Transformable;
