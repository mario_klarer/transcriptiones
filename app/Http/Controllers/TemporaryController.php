<?php

namespace App\Http\Controllers;

use App\Models\Line;

class TemporaryController extends Controller
{
    public function __invoke()
    {
        $line = Line::find(16);

        return view('presentation.test')
            ->with('test', $line->value);
    }
}
