<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class LoginController extends Controller
{
    public function index() {
        return view('auth.login');
    }

    public function store(LoginRequest $request) {
        $data = $request->validated();

        if(Auth::attempt($data)) {
            return redirect()->route('dashboard');
        }

        return redirect()->back()
            ->withErrors([Config::get('app.error_key') => 'Wir konnten die Daten nicht im System finden.'])->withInput(
            $request->except('password')
        );
    }
}
