<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\InquireResetPasswordRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Models\User;
use App\Notifications\Auth\ResetPasswordMail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    public function index() {
        return view('auth.inquire-reset');
    }

    public function indexReset(Request $request) {
        $token = $request->query('token');
        $email = $request->query('email');

        return view('auth.reset')
            ->with('email', $email)
            ->with('token', $token);
    }

    public function store(InquireResetPasswordRequest $request) {
        $data = $request->validated();

        $user = User::where('email', '=', $data['email'])->first();

        if(!$user) {
            return redirect()->back()
                ->withErrors([Config::get('app.error_key') => 'Wir konnten die Daten nicht im System finden.'])->withInput();
        }

        $token = Str::random(128);
        $user->password_reset_token = $token;
        $user->password_token_valid_until = Carbon::now()->addHour();
        $user->save();

        $user->notify(new ResetPasswordMail($token, $user->email));

        return redirect()->route('login')->with(Config::get('app.success_key'), 'Wir haben dir eine E-Mail gesendet.');
    }

    public function update(ResetPasswordRequest $request) {
        $data = $request->validated();

        $user = User::where('email', '=', $data['email'])->first();

        if(!$user
            || $user->password_reset_token === NULL
            || $user->password_reset_token !== $data['token']
            || $user->password_token_valid_until === NULL
            || $user->password_token_valid_until->lt(Carbon::now())) {
            return edirect()->back()
                ->withErrors([Config::get('app.error_key') => 'Wir konnten die Daten nicht im System finden.']);
        }

        $user->password = Hash::make($data['password']);
        $user->password_reset_token = NULL;
        $user->password_token_valid_until = NULL;
        $user->save();

        return redirect()->route('login')->with(Config::get('app.success_key'), 'Dein Passwort wurde erfolgreich zurück gesetzt.');
    }
}
