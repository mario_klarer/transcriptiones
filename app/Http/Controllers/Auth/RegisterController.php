<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index() {
        return view('auth.register');
    }

    public function store(RegisterRequest $request) {
        $data = $request->validated();

        if(Config::get('app.disable_login'))
            return redirect()->back()
                ->withErrors([Config::get('app.error_key') => 'Registrierung ist momentan deaktiviert.'])->withInput(
                    $request->except('password')
                );

        $user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        Auth::loginUsingId($user->id);

        return redirect()->route('dashboard');
    }
}
