<?php

namespace App\Http\Controllers\Search;

use App\Exports\SearchExport;
use App\Http\Controllers\Controller;
use App\Models\Line;
use App\Models\SearchString;
use App\Services\Transkribus\TranskribusService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class SearchController extends Controller
{
    private TranskribusService $transkribusService;

    public function __construct(TranskribusService $transkribusService)
    {
        $this->transkribusService = $transkribusService;
    }

    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search' => 'nullable|string',
            'full_word' => ['nullable', 'string', 'regex:/^(true|false)*$/'],
            'normalized' => ['nullable', 'string', 'regex:/^(true|false)*$/'],
            'export' => ['nullable', 'string', 'regex:/^(true|false)*$/'],
        ]);

        $view = view('search.index');

        if ($validator->fails()) {
            return $view->with('search', '')->with('fullWord', false)->with('normalized', false)->with('lines', collect([]));
        }

        $search = $request->query('search') ?? '';

        $search = str_replace('​', '', $search);

        $fullWord = ($request->query('full_word') ?? 'false') === 'true';
        $normalized = ($request->query('normalized') ?? 'false') === 'true';
        $export = ($request->query('export') ?? 'false') === 'true';

        if($normalized)
            $search = $this->transkribusService->removePunctures($this->transkribusService->normalizeString($search));

        $lines = SearchString::join('chapters', 'chapter_id', '=', 'chapters.id')
            ->where(function($query) use ($search, $fullWord, $normalized) {
                if($fullWord) {
                    $query->where($normalized ? 'normalized_value' : 'value', 'LIKE', '% ' . ($search) . ' %')
                          ->orWhere($normalized ? 'normalized_value' : 'value', 'LIKE', ($search) . ' %')
                          ->orWhere($normalized ? 'normalized_value' : 'value', 'LIKE', '% ' . ($search));
                } else {
                    $term = '%' . ($search) . '%';

                    $query->where($normalized ? 'normalized_value' : 'value', 'LIKE', $term);
                }
            })
            ->orderBy('chapter_name', 'ASC')
            ->orderBy('strophe_reading_number', 'ASC')
            ->orderBy('verse_reading_number', 'ASC');

        $totalCount = 0;

        if(!$export) {
            $totalCount = $lines->count();
            $lines = $lines->simplePaginate(20);
        } else {
            return Excel::download(new SearchExport($lines->get(), $search, $normalized, $fullWord), 'search-result.csv');
        }

        return view('search.index')
            ->with('totalCount', $totalCount)
            ->with('normalized', $normalized)
            ->with('lines', $lines)
            ->with('search', $search)
            ->with('fullWord', $fullWord);
    }
}
