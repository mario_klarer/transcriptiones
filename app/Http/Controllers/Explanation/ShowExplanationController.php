<?php

namespace App\Http\Controllers\Explanation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShowExplanationController extends Controller
{
    public function __invoke()
    {
        return view('explanation.index');
    }
}
