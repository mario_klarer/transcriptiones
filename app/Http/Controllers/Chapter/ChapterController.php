<?php

namespace App\Http\Controllers\Chapter;

use App\Http\Controllers\Controller;
use App\Models\Chapter;
use Illuminate\Http\Request;

class ChapterController extends Controller
{
    public function __invoke()
    {
        $chapters = Chapter::with('pages')->get();

        return view('chapter.index')->with('chapters', $chapters);
    }
}
