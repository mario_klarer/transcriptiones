<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use App\Models\Chapter;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    public function __invoke(Page $page, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search' => 'nullable|string',
            'full_word' => ['nullable', 'boolean'],
            'normalized' => ['nullable', 'boolean'],
            'verse' => ['nullable', 'integer'],
        ]);

        if ($validator->fails()) {
            $search = '';
            $fullWord = false;
            $normalized = false;
            $preselectedVerse = NULL;
        } else {
            $search = $request->query('search') ?? '';
            $fullWord = ($request->query('full_word') ?? "0") === "1";
            $normalized = ($request->query('normalized') ?? "0") === "1";
            $preselectedVerse = $request->query('verse') ?? NULL;
        }

        $page->load(['previous_page', 'next_page', 'text_areas.lines']);
        $currentChapter = Chapter::where('id', '=', $page->chapter_id)
            ->with(['next_chapter', 'previous_chapter'])
            ->first();

        $verses = [];
        $strophes = [];

        foreach($page->text_areas as $textArea) {
            $verses = array_merge($verses, $textArea->verses()->with('strophe')->get()->toArray());
            $strophes = array_merge($strophes, $textArea->strophes()->with('verses')->get()->toArray());
        }

        $page->verses = $verses;
        $page->strophes = $strophes;

        $allPagesOfChapter = $currentChapter->pages;
        $allChapters = Chapter::all();

        return view('page.show')
            ->with('page', $page)
            ->with('currentChapter', $currentChapter)
            ->with('allPagesOfChapter', $allPagesOfChapter)
            ->with('allChapters', $allChapters)
            ->with('fullWord', $fullWord)
            ->with('normalized', $normalized)
            ->with('preselectedVerse', $preselectedVerse)
            ->with('search', $search);
    }
}
