<?php

namespace App\Http\Controllers\Legal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LegalController extends Controller
{
    public function imprint() {
        return view('presentation.imprint');
    }

    public function terms() {
        return view('presentation.terms');
    }

    public function dataprotection() {
        return view('presentation.data-protection');
    }
}
