<?php

namespace App\Objects\ExceptionCase;

use App\Exceptions\ExceptionCaseCouldNotBeAppliedException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;

class AddAmenToVersePageFourHundredEightyFour extends ExceptionCase {
    public function __invoke() {
        $verse = Verse::where('value', '=', ' nach diſem lebeŋ ward gegebŋ̄ ')->first();

        throw_if(!$verse, ExceptionCaseCouldNotBeAppliedException::class, 'Verse not found.');

        $values = [
            'name' => Transformation::amen,
            'transformable_type' => Verse::class,
            'transformable_id' => $verse->id,
            'start' => 0,
            'end' => 31,
            'order' => 99999,
            'normalized' => 0,
        ];

        Transformation::create($values);
        $values['normalized'] = 1;
        Transformation::create($values);
    }
}
