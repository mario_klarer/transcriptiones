<?php

namespace App\Objects\ExceptionCase;

use App\Exceptions\ExceptionCaseCouldNotBeAppliedException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Transformation;
use App\Models\TransformationValue;

class MoveBigInitialPageNinteen extends ExceptionCase {
    public function __invoke() {
        $line = Line::where('value', '=', 'Alɞ eiŋ fraw hat beia⸗')->first();

        throw_if(!$line, ExceptionCaseCouldNotBeAppliedException::class, 'Line not found.');

        $option = $line->options()->where('name', '=', Option::bigInitial)->first();
        $transformation = $line->transformations()->where('name', '=', Transformation::bigInitial)->first();

        throw_if(!$option, ExceptionCaseCouldNotBeAppliedException::class, 'Big Init Option not found.');
        throw_if(!$transformation, ExceptionCaseCouldNotBeAppliedException::class, 'Big Init Transformation not found.');

        $optionValue = $option->option_values()->where('key', '=', OptionValue::offsetText)->first();
        $transformationValue = $transformation->transformation_values()->where('key', '=', TransformationValue::offsetText)->first();

        throw_if(!$optionValue, ExceptionCaseCouldNotBeAppliedException::class, 'Offset Text Option Value not found.');
        throw_if(!$transformationValue, ExceptionCaseCouldNotBeAppliedException::class, 'Offset Text Transformation Value not found.');

        $optionValue->value = 2;
        $optionValue->save();
        $transformationValue->value = 2;
        $transformationValue->save();
    }
}
