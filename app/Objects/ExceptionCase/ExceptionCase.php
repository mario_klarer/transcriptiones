<?php

namespace App\Objects\ExceptionCase;

abstract class ExceptionCase {
    abstract public function __invoke();
}
