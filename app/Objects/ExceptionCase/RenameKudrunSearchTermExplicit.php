<?php

namespace App\Objects\ExceptionCase;

use App\Exceptions\ExceptionCaseCouldNotBeAppliedException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Page;
use App\Models\SearchString;
use App\Models\Strophe;
use App\Models\TextArea;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;
use Illuminate\Support\Facades\Log;

class RenameKudrunSearchTermExplicit extends ExceptionCase {
    public function __invoke() {
        $searchString = SearchString::firstWhere('value', '=', 'Hie hat Chautrum ein ennde');

        $searchString->strophe_number = NULL;
        $searchString->verse_number = 'Explicit';
        $searchString->save();
    }
}
