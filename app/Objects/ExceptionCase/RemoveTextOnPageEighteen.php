<?php

namespace App\Objects\ExceptionCase;

use App\Exceptions\ExceptionCaseCouldNotBeAppliedException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Page;
use App\Models\TextArea;
use App\Models\Transformation;
use App\Models\TransformationValue;

class RemoveTextOnPageEighteen extends ExceptionCase {
    public function __invoke() {
        $page = Page::where('page_number', '=', 18)->first();

        throw_if(!$page, ExceptionCaseCouldNotBeAppliedException::class, 'Page not found.');

        $textAreas = TextArea::where('page_id', '=', $page->id)->get();

        foreach($page->search_strings as $searchString) {
            $searchString->delete();
        }

        foreach($textAreas as $textArea) {
            $textArea->lines()->delete();
            $textArea->verses()->delete();

            $textArea->delete();
        }
    }
}
