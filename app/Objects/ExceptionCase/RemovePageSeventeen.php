<?php

namespace App\Objects\ExceptionCase;

use App\Exceptions\ExceptionCaseCouldNotBeAppliedException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Page;
use App\Models\SearchString;
use App\Models\TextArea;
use App\Models\Transformation;
use App\Models\TransformationValue;

class RemovePageSeventeen extends ExceptionCase {
    public function __invoke() {
        $page = Page::where('page_number', '=', 17)->first();
        $pageSixteen = Page::where('page_number', '=', 16)->first();
        $pageEighteen = Page::where('page_number', '=', 18)->first();

        throw_if(!$page, ExceptionCaseCouldNotBeAppliedException::class, 'Page not found.');

        $textAreas = TextArea::where('page_id', '=', $page->id)->get();

        foreach($page->search_strings as $searchString) {
            $searchString->delete();
        }

        foreach($textAreas as $textArea) {
            $textArea->lines()->delete();
            $textArea->verses()->delete();

            $textArea->delete();
        }

        $searchStrings = SearchString::where('page_id', '=', $page->id)->get();

        foreach($searchStrings as $searchString) {
            $searchString->delete();
        }

        Page::where('next_page_id', '=', $page->id)->update(['next_page_id' => $pageEighteen->id]);
        Page::where('previous_page_id', '=', $page->id)->update(['previous_page_id' => $pageSixteen->id]);

        $page->delete();
    }
}
