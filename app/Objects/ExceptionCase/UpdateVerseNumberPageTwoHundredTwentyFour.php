<?php

namespace App\Objects\ExceptionCase;

use App\Exceptions\ExceptionCaseCouldNotBeAppliedException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;

class UpdateVerseNumberPageTwoHundredTwentyFour extends ExceptionCase {
    public function __invoke() {
        $verse = Verse::where('value', '=', ' maŋ truͦg auch darmit Schilde ỽnd ỽil mani⸗gen eyſnen ſcɧafft ·')->first();

        throw_if(!$verse, ExceptionCaseCouldNotBeAppliedException::class, 'Verse not found.');

        $verse->verse_number = 4;
        $verse->save();
    }
}
