<?php

namespace App\Objects\ExceptionCase;

use App\Exceptions\ExceptionCaseCouldNotBeAppliedException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\TextArea;
use App\Models\Transformation;
use App\Models\TransformationValue;

class MoveFirstIncipitOfTabula extends ExceptionCase {
    public function __invoke() {
        $line = Line::where('value', '=', 'Tabŭla deɞ Heldenpuͦchs')->first();

        throw_if(!$line, ExceptionCaseCouldNotBeAppliedException::class, 'Line not found.');

        $textarea = TextArea::find($line->text_area_id + 1);

        $textarea->lines()->whereNotNull('line_number')->increment('line_number');

        $line->text_area_id = $line->text_area_id + 1;
        $line->save();
    }
}
