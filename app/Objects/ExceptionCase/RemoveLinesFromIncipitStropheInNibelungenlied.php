<?php

namespace App\Objects\ExceptionCase;

use App\Exceptions\ExceptionCaseCouldNotBeAppliedException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Page;
use App\Models\SearchString;
use App\Models\Strophe;
use App\Models\TextArea;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;
use Illuminate\Support\Facades\Log;

class RemoveLinesFromIncipitStropheInNibelungenlied extends ExceptionCase {
    public function __invoke() {
        $pagesAndValuesToDelete = [
            [
                'pageNumber' => 224,
                'values' => [
                    'Annderthalbeŋ deɞ Reineɞ ſach maŋ mit mani⸗geŋ charŋ ',
                    'deŋ kuᷓnig mit ſeineŋ geſteŋ zu dem geſtatte varŋ ',
                    'auch ſach man dabeÿ zaume laitŋ̄ mange maid ',
                    'die Sy emphaheŋ ſol⸗teŋ · die wareŋ alle berait ·',
                ],
            ],
            [
                'pageNumber' => 230,
                'values' => [
                    'Alle Jr vnmuͦσ  die laſſeŋ wir nu ſein ',
                    ' ỽnnd ſageŋ ⱳie fraŭ Chrimhilt vnd auch jr mage dein ',
                    ' gegeŋ Rein fuͦren  ỽon Nybelunge lanndt ·',
                    ' nie getruͦgŋ̄ mere  ſo manig reich gewant ·',
                ],
            ],
            [
                'pageNumber' => 225,
                'values' => [
                    'Wie Praŭnhilt ze Wŭrmbs Emphanngen ward ',
                ],
            ],
            [
                'pageNumber' => 231,
                'values' => [
                    'Abentheur · Wie Seÿfrid mit ſeinem weybe zu der hochʒeit fuͦr · ',
                ],
            ],
        ];

        foreach($pagesAndValuesToDelete as $toDelete) {
            $page = Page::where('page_number', '=', $toDelete['pageNumber'])->first();

            throw_if(!$page, ExceptionCaseCouldNotBeAppliedException::class, 'Page not found.');

            $textAreaIds = $page->text_areas->pluck('id');

            $stropheIds = Strophe::whereIn('text_area_id', $textAreaIds)->pluck('id');

            $verseValuesToDelete = $toDelete['values'];

            foreach($verseValuesToDelete as $verseValue) {
                $verse = Verse::where('value', 'LIKE', '%' . $verseValue . '%')->whereIn('strophe_id', $stropheIds)->first();

                if($verse) {
                    $verse->deleteWithForeign();
                }
            }
        }
    }
}
