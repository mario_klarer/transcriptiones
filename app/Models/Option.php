<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    const readingOrder = "readingOrder";
    const abbreviation = "abbrev";
    const endOfVerse = "endOfVerse";
    const endOfStrophe = "endOfStrophe";
    const incipit = "incipit";
    const explicit = "explicit";
    const startText = "startText";
    const hyphenation = "hyphenation";
    const caesura = "caesura";
    const aventiure = "startAventiure";
    const redInitial = "redInitial";
    const blueInitial = "blueInitial";
    const redRubric = "redRubric";
    const bigInitial = "bigInitial";
    const missingInitial = "missingInitial";
    const strikethrough = "textStyle";
    const amen = "amen";
    const insertion = "insertion";

    use HasFactory;

    protected $fillable = [
        'name',
        'optionable_id',
        'optionable_type',
    ];

    public function option_values() {
        return $this->hasMany( OptionValue::class, 'option_id', 'id');
    }

    public function optionable()
    {
        return $this->morphTo();
    }

    public function deleteWithForeign() {
        $this->option_values()->delete();

        $this->delete();
    }
}
