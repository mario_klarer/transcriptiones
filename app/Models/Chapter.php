<?php

namespace App\Models;

use App\Services\Transkribus\TranskribusService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'next_chapter_id', 'is_absolute', 'has_strophes', 'previous_chapter_id'];

    protected $appends = ['link'];

    public function next_chapter() {
        return $this->belongsTo( Chapter::class, 'next_chapter_id', 'id');
    }

    public function previous_chapter() {
        return $this->belongsTo( Chapter::class, 'previous_chapter_id', 'id');
    }

    public function search_strings() {
        return $this->hasMany( SearchString::class, 'chapter_id', 'id');
    }

    public function next_chapters() {
        return $this->hasMany( Chapter::class, 'previous_chapter_id', 'id');
    }

    public function previous_chapters() {
        return $this->hasMany( Chapter::class, 'next_chapter_id', 'id');
    }

    public function pages() {
        return $this->hasMany( Page::class, 'chapter_id', 'id');
    }

    public function getTeaserImageLinkAttribute() {
        $service = app()->get(TranskribusService::class);

        $firstPage = $this->pages()->orderBy('page_number')->first();

        $fileName = $service->getFileNameFromFileNumber($firstPage->page_number, 'jpg');

        return safeMix('/assets/pages/' . $fileName);
    }

    public function getFirstThreeLinesAttribute() {
        $firstPage = $this->pages()->orderBy('page_number')->first();
        $firstTextArea = $firstPage->text_areas()->whereHas('lines', function($q) {
            $q->whereNotNull('value')->where('value', '<>', '');
        })->orderBy('reading_number')->first();

        return $firstTextArea->lines()->whereNotNull('value')->where('value', '<>', '')->take(3)->get();
    }

    public function getFirstPageAttribute() {
        $firstPage = $this->pages()->orderBy('page_number')->first();

        return $firstPage;
    }

    public function getLinkAttribute() {
        $firstPage = $this->first_page;

        return $firstPage->link;
    }
}
