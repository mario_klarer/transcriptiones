<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Strophe extends Model
{
    use HasFactory;

    protected $fillable = [
        'text_area_id',
        'reading_number',
        'strophe_number'
    ];

    public function text_area() {
        return $this->belongsTo( TextArea::class, 'text_area_id', 'id');
    }

    public function verses() {
        return $this->hasMany( Verse::class, 'strophe_id', 'id');
    }
}
