<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Verse extends Transformable
{
    use HasFactory;

    protected $fillable = [
        'text_area_id',
        'strophe_id',
        'reading_number',
        'verse_number',
        'value',
        'min_x',
        'min_y',
        'max_x',
        'max_y',
    ];

    public function strophe() {
        return $this->belongsTo( Strophe::class, 'strophe_id', 'id');
    }

    public function text_area() {
        return $this->belongsTo( TextArea::class, 'text_area_id', 'id');
    }

    public function search_strings() {
        return $this->hasMany( SearchString::class, 'verse_id', 'id');
    }

    public function options()
    {
        return $this->morphMany(Option::class, 'optionable');
    }

    public function deleteWithForeign() {
        foreach($this->options as $option) {
            $option->deleteWithForeign();
        }

        foreach($this->transformations as $transformation) {
            $transformation->deleteWithForeign();
        }

        foreach($this->search_strings as $searchString) {
            $searchString->delete();
        }

        $this->delete();
    }
}
