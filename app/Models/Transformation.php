<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transformation extends Model
{
    use HasFactory;

    public const incipit = "incipit";
    public const caesura = "caesura";
    public const aventiure = "aventiure";
    public const abbreviation = "abbreviation";
    const redInitial = "redInitial";
    const blueInitial = "blueInitial";
    const redRubric = "redRubric";
    const bigInitial = "bigInitial";
    const missingInitial = "missingInitial";
    const strikethrough = "strikethrough";
    const amen = "amen";
    const insertion = "insertion";

    protected $fillable = ['name', 'start', 'end', 'order', 'normalized', 'transformable_type', 'transformable_id'];

    public function transformable()
    {
        return $this->morphTo();
    }

    public function transformation_values() {
        return $this->hasMany( TransformationValue::class, 'transformation_id', 'id');
    }

    public function deleteWithForeign() {
        $this->transformation_values()->delete();

        $this->delete();
    }
}
