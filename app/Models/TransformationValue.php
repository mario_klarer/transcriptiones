<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransformationValue extends Model
{
    use HasFactory;

    public const aventiureValue = "aventiureValue";
    public const specialAventiure = "specialAventiure";
    public const expansion = "expansion";
    public const height = "height";
    public const offsetText = "offsetText";
    public const isThinInitial = "isThinInitial";
    public const insert = "insert";

    protected $fillable = ['key', 'value', 'transformation_id'];

    public function transformation() {
        return $this->belongsTo( Transformation::class, 'transformation_id', 'id');
    }
}
