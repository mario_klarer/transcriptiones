<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SearchString extends Model
{
    use HasFactory;

    protected $fillable = [
        'chapter_id',
        'page_id',
        'verse_id',
        'chapter_name',
        'strophe_number',
        'strophe_reading_number',
        'verse_number',
        'verse_reading_number',
        'value',
        'normalized_value',
    ];

    public function chapter() {
        return $this->belongsTo( Chapter::class, 'chapter_id', 'id');
    }

    public function page() {
        return $this->belongsTo( Page::class, 'page_id', 'id');
    }
}
