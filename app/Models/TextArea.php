<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TextArea extends Model
{
    use HasFactory;

    protected $fillable = ['page_id', 'reading_number', 'min_x', 'min_y', 'max_x', 'max_y'];

    public function page() {
        return $this->belongsTo( Page::class, 'page_id', 'id');
    }

    public function lines() {
        return $this->hasMany( Line::class, 'text_area_id', 'id');
    }

    public function strophes() {
        return $this->hasMany( Strophe::class, 'text_area_id', 'id');
    }

    public function verses() {
        return $this->hasMany( Verse::class, 'text_area_id', 'id');
    }

    public function options()
    {
        return $this->morphMany(Option::class, 'optionable');
    }
}
