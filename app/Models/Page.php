<?php

namespace App\Models;

use App\Services\Transkribus\TranskribusService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable = ['chapter_id', 'next_page_id', 'previous_page_id', 'folio', 'page_number', 'image_height', 'image_width'];

    protected $appends = ['link', 'image_link'];

    public function chapter() {
        return $this->belongsTo( Chapter::class, 'chapter_id', 'id');
    }

    public function next_page() {
        return $this->belongsTo( Page::class, 'next_page_id', 'id');
    }

    public function previous_page() {
        return $this->belongsTo( Page::class, 'previous_page_id', 'id');
    }

    public function next_pages() {
        return $this->hasMany( Page::class, 'previous_page_id', 'id');
    }

    public function previous_pages() {
        return $this->hasMany( Page::class, 'next_page_id', 'id');
    }

    public function text_areas() {
        return $this->hasMany( TextArea::class, 'page_id', 'id');
    }

    public function search_strings() {
        return $this->hasMany( SearchString::class, 'page_id', 'id');
    }

    public function getLinkAttribute() {
        return route('page.show', ['page' => $this->id]);
    }

    public function getImageLinkAttribute() {
        $transkribusService = app()->get(TranskribusService::class);

        return safeMix('/assets/pages/' . $transkribusService->getFileNameFromFileNumber($this->page_number, 'jpg'));
    }
}
