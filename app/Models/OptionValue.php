<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptionValue extends Model
{
    public const offset = "offset";
    public const length = "length";
    public const value = "value";
    public const continued = "continued";
    public const expansion = "expansion";
    public const height = "height";
    public const offsetText = "offsetText";
    public const insert = "insert";

    use HasFactory;

    protected $fillable = [
        'key',
        'value',
        'option_id',
    ];

    public function option() {
        return $this->belongsTo( Option::class, 'option_id', 'id');
    }
}
