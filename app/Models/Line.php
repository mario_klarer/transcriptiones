<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Line extends Transformable
{
    use HasFactory;

    protected $fillable = ['text_area_id', 'value', 'reading_number', 'line_number', 'line_indent', 'small_indent', 'min_x', 'min_y', 'max_x', 'max_y'];

    public function text_area() {
        return $this->belongsTo( TextArea::class, 'text_area_id', 'id');
    }

    public function options()
    {
        return $this->morphMany(Option::class, 'optionable');
    }
}
