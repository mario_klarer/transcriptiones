<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transformable extends Model
{
    use HasFactory;

    protected $appends = ['transformation_options', 'transformation_options_normalized'];

    public function transformations()
    {
        return $this->morphMany(Transformation::class, 'transformable');
    }

    private function transformationOptionArray(bool $normalize) {
        $transfromations = $this->transformations()->orderBy('order', 'ASC')->where('normalized', '=', $normalize ? '1' : '0')->get();

        $options = [];

        foreach($transfromations as $transfromation) {
            $values = $transfromation->transformation_values;
            $currentOptionValues = [];

            foreach($values as $value) {
                $currentOptionValues[$value->key] = $value->value;
            }

            $options[] = [
                'name' => $transfromation->name,
                'start' => $transfromation->start,
                'end' => $transfromation->end,
                'values' => $currentOptionValues,
            ];
        }

        return $options;
    }

    public function getTransformationOptionsAttribute() {
        return $this->transformationOptionArray(false);
    }

    public function getTransformationOptionsNormalizedAttribute() {
        return $this->transformationOptionArray(true);
    }

    public function containsBigInitialLetter() {
        return $this->transformations()->where('name', '=', Option::bigInitial)->count() > 0
            || $this->transformations()->where('name', '=', Option::redInitial)->count() > 0
            || $this->transformations()->where('name', '=', Option::blueInitial)->count() > 0
            || $this->transformations()->where('name', '=', Option::missingInitial)->count() > 0;
    }
}
