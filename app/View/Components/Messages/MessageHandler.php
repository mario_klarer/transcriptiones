<?php

namespace App\View\Components\Messages;

use Illuminate\View\Component;

class MessageHandler extends Component
{
    public $errors;
    public $success;
    public $messages;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($errors, $success)
    {
        $this->messages = $this->resolveMessages($errors, $success);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.messages.message-handler');
    }

    private function resolveMessages($errors, $success)
    {
        $stm = collect();

        if($errors) {
            $stm->put(config('app.error_key'), $errors->all());
        }

        if($success) {
            $stm->put(config('app.success_key'), $success);
        }

        return $stm;
    }
}
