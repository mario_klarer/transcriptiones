<?php

namespace App\View\Components\Essentials;

use Illuminate\View\Component;

class Card extends Component
{
    private ?string $imgLink;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(?string $imgLink = NULL)
    {
        $this->imgLink = $imgLink;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.essentials.card')
            ->with('imgLink', $this->imgLink);
    }
}
