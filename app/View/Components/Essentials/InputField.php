<?php

namespace App\View\Components\Essentials;

use Illuminate\View\Component;

class InputField extends Component
{
    private ?string $label;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(?string $label)
    {
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.essentials.input-field')
            ->with('label', $this->label);
    }
}
