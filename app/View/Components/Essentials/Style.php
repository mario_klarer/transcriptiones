<?php

namespace App\View\Components\Essentials;

use Illuminate\View\Component;

class Style extends Component
{
    private const globalStyles = array('css/app.css', 'css/fonts.css');
    public $styles;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(array $styles = array())
    {
        $this->styles = array_merge($styles, self::globalStyles);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.essentials.style');
    }
}
