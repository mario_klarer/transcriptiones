<?php

namespace App\View\Components\Essentials;

use Illuminate\View\Component;

class Javascript extends Component
{
    private const standardScriptsPreLoad = array('js/app.js');
    private const standardScriptsPostLoad = array('js/vue-init.js');

    public $scripts;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(array $scripts = array())
    {
        $this->scripts = array_merge(self::standardScriptsPreLoad, $scripts, self::standardScriptsPostLoad);
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.essentials.javascript');
    }
}
