<?php

namespace App\View\Components\Essentials;

use Illuminate\View\Component;

class Head extends Component
{
    public $title;
    public $description;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title = 'Transcriptiones.com', $description = 'Das ÖAW-go!digital-2.0-Projekt "Ambraser Heldenbuch: Transkription und wissenschaftliches Datenset" setzt sich zum Ziel das Ambraser Heldenbuch (Wien, Österr. Nationalbibl., Cod. Ser. nova 2663) aus dem frühen 16. Jahrhundert zur Gänze zu transkribieren. Das Ambraser Heldenbuch wurde am Beginn des 16. Jahrhunderts von Kaiser Maximilian I. als Prunkhandschrift in Auftrag gegeben und vom Bozner Zöllner Hans Ried in einer Hand auf ca. 250 großformatigen Pergamentblättern ausgeführt. In diesem Prachtkodex finden sich 25 wichtige mittelhochdeutsche literarische Texte (z.B. Nibelungenlied, Helmbrecht etc.), wovon 15 als Unikate (z.B. Erec, Moriz von Craûn etc.) ausschließlich im Ambraser Heldenbuch überliefert sind.')
    {
        $this->title = $title;
        $this->description = $description;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.essentials.head');
    }
}
