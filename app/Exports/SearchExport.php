<?php

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;

class SearchExport implements FromArray
{
    protected Collection $searchResult;
    protected string $search;
    protected string $normalized;
    protected string $fullWord;

    public function __construct(Collection $searchResult, string $search, bool $normalized, bool $fullWord)
    {
        $this->searchResult = $searchResult;
        $this->search = $search;
        $this->normalized = $normalized ? '1' : '0';
        $this->fullWord = $fullWord ? '1' : '0';
    }

    public function array(): array
    {
        $header = ['Text', 'Strophe/Vers', 'Link'];
        return [$header, ...$this->searchResult->map(fn($x) => collect([$x->page->chapter->name, str_replace('<i>', '', str_replace('</i>', '', $x->strophe_number !== null ? ($x->strophe_number . ',' . $x->verse_number) : $x->verse_number)), route('page.show', ['page' => $x->page->id, 'search' => $this->search, 'verse' => $x->verse_id, 'normalized' => $this->normalized, 'full_word' => $this->fullWord])]))->toArray()];
    }
}
