<?php

namespace App\Jobs;

use App\Exceptions\ExceptionCaseCouldNotBeAppliedException;
use App\Models\Chapter;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\SearchString;
use App\Models\Strophe;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;
use App\Objects\ExceptionCase\AddAmenToVersePageFourHundredEightyFour;
use App\Objects\ExceptionCase\MoveBigInitialPageNinteen;
use App\Objects\ExceptionCase\MoveFirstIncipitOfTabula;
use App\Objects\ExceptionCase\RemoveLinesFromIncipitStropheInNibelungenlied;
use App\Objects\ExceptionCase\RemovePageSeventeen;
use App\Objects\ExceptionCase\RemoveTextOnPageEighteen;
use App\Objects\ExceptionCase\RenameKudrunSearchTermExplicit;
use App\Objects\ExceptionCase\UpdateVerseNumberPageTwoHundredTwentyFour;
use App\Services\Chapter\ChapterService;
use App\Services\Csv\CsvService;
use App\Services\Line\LineService;
use App\Services\Option\OptionService;
use App\Services\Page\PageService;
use App\Services\TextRegion\TextAreaService;
use App\Services\Transformation\ApplyTransformationsService;
use App\Services\Transkribus\TranskribusService;
use App\Services\Verse\VerseService;
use App\Services\Xml\TranskribusXmlService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ParseTextsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(CsvService $csvService,
                           TranskribusService $transkribusService,
                           ChapterService $chapterService,
                           PageService $pageService,
                           TranskribusXmlService $xmlService,
                           TextAreaService $textAreaService,
                           LineService $lineService,
                           VerseService $verseService,
                           ApplyTransformationsService $applyTransformationsService,
                           OptionService $optionService)
    {
        $texts = $csvService->csvToArray(resource_path('csv/texts.csv'));
        $previousChapter = NULL;

        foreach($texts as $text) {
            $isTextAbsolute = $text['absolute'] === "true";

            $fileStartNumber = (int) $text['image_start'];
            $fileEndNumber = (int) $text['image_end'];

            $fileNumbers = range($fileStartNumber, $fileEndNumber);

            $hasStrophes = $text['strophes'] === 'true';

            $chapter = $chapterService->createChapter($text['title'], $previousChapter, $hasStrophes, $isTextAbsolute);

            if($previousChapter !== NULL) {
                $previousChapter = $chapterService->addNextChapter($previousChapter, $chapter);
                $previousChapter->save();
            }

            $previousPage = NULL;

            // Verse Parameters
            $currentAllographicVerse = '';
            $currentStropheReadingNumber = 0;
            $currentVerseReadingNumber = 0;
            $currentStrophe = NULL;
            $currentVerseMinX = 9999999;
            $currentVerseMinY = 9999999;
            $currentVerseMaxX = -1;
            $currentVerseMaxY = -1;
            $currentVerseCoords = ['minX' => [], 'minY' => [],'maxX' => [], 'maxY' => [],];
            $previousPageVerseCoords = ['minX' => [], 'minY' => [],'maxX' => [], 'maxY' => [],];
            $currentVerseLines = [];
            $previousLineContinued = false;
            $previousLineIncipit = false;
            $copyVerseToPreviousPage = false;

            // Text Parameters
            $foundStartText = $isTextAbsolute;
            $foundEndText = false;

            foreach($fileNumbers as $fileNumber) {
                if($foundEndText)
                    break;

                $xml = $transkribusService->loadXml($fileNumber);

                $folio = $xmlService->getFolio($xml);

                $imageSize = $xmlService->getImageSize($xml);

                $page = $pageService->createPage($chapter, $previousPage, $folio, $fileNumber, $imageSize);

                if($previousPage !== NULL) {
                    $previousPage = $pageService->addNextPage($previousPage, $page);
                    $previousPage->save();
                }

                $textRegions = $xmlService->getTextRegions($xml);

                foreach($textRegions as $key => $region) {
                    if($foundEndText || ($isTextAbsolute && $key > 1 && $fileNumber !== 9))
                        break;

                    $emptyLinesCount = 0;
                    $lineIndent = NULL;
                    $linesToIndent = 0;
                    $smallIndent = false;

                    $textAreaCoords = $xmlService->getCoords($region);
                    $textArea = $textAreaService->createTextArea($page, $key, $textAreaCoords);

                    if(isset($currentStrophe) && $currentStrophe->verses()->count() === 0 && trim($currentAllographicVerse) === "") {
                        $currentStrophe->text_area_id = $textArea->id;
                        $currentStrophe->save();
                    }

                    if(!$isTextAbsolute && $currentStrophe === NULL && $chapter->has_strophes) {
                        $currentStrophe = Strophe::create([
                            'text_area_id' => $textArea->id,
                            'reading_number' => $currentStropheReadingNumber,
                        ]);
                    }

                    $lines = $xmlService->getLinesWithOptionsAndCoordsFromArea($region);

                    foreach($lines as $lineIndex => $lineArray) {
                        $verseLineOffset = 0;

                        if(!$isTextAbsolute) {
                            if($foundStartText && isset($lineArray['options'][Option::startText])) {
                                $foundEndText = true;
                                break;
                            }

                            if(isset($lineArray['options'][Option::startText])) {
                                $foundStartText = true;
                            }
                        }

                        if(!$foundStartText || $lineArray['line'] == '') {
                            $lineValue = NULL;
                        } else {
                            $lineValue = $lineArray['line'];
                        }

                        if(!$isTextAbsolute) {

                            // Empty lines before text starts should not be counted
                            if($lineArray['line'] == '')
                                $emptyLinesCount++;

                            // Check if Initial is present and determine how far to indent
                            $heightValueOfInitial = NULL;
                            $offsetValueOfInitial = NULL;
                            foreach($lineArray['options'] as $key => $options) {
                                if($key === Option::bigInitial || $key === Option::redInitial || $key === Option::blueInitial) {
                                    foreach($options as $option) {
                                        foreach($option as $valKey => $value) {
                                            if($valKey === OptionValue::height) {
                                                $heightValueOfInitial = $value;
                                            }

                                            if($valKey === OptionValue::offsetText) {
                                                $offsetValueOfInitial = $value;
                                            }
                                        }
                                    }
                                }
                            }

                            try {
                                if($heightValueOfInitial !== NULL) {
                                    $linesToIndent = $heightValueOfInitial - ($offsetValueOfInitial ?? 0);
                                    $lineIndent = $heightValueOfInitial;
                                    if(str_starts_with($lineArray['line'], 'I') || str_starts_with($lineArray['line'], 'J'))
                                        $smallIndent = true;

                                    if($offsetValueOfInitial !== NULL) {
                                        $lines = $textArea->lines()->orderBy('lines.id', 'desc')->take($offsetValueOfInitial)->pluck('lines.id');

                                        foreach($lines as $lineId) {
                                            Line::find($lineId)->update([
                                                'line_indent' => $heightValueOfInitial,
                                                'small_indent' => $smallIndent,
                                            ]);
                                        }
                                    }
                                }
                            } catch (\Throwable $e) {
                                // Do nothing.
                            }
                        }

                        $line = $lineService->createLine(
                            $textArea,
                            $lineArray['readingNumber'],
                            $lineArray['coords'],
                            $lineValue,
                            $emptyLinesCount,
                            $lineIndent,
                            $smallIndent,
                            $isTextAbsolute,
                        );

                        if($isTextAbsolute && $lineService->isPageNumber($lineValue))
                            $emptyLinesCount++;

                        $linesToIndent = max(0, $linesToIndent - 1);

                        if($linesToIndent === 0) {
                            $lineIndent = NULL;
                            $smallIndent = false;
                        }

                        foreach($lineArray['options'] as $key => $options) {
                            foreach($options as $option) {
                                $optionService->createOption($key, $line, $option);
                            }
                        }

                        if($lineValue === NULL)
                            continue;

                        $applyTransformationsService->createTransformations($line);

                        # if($isTextAbsolute)
                        #    continue;

                        if($foundEndText)
                            continue;

                        $verseSplit = $verseService->splitLineIntoVerseParts($line, $previousLineIncipit);

                        try {
                            $currentLineIncipit = $verseSplit['allographic'][0]['incipit'];
                        } catch (\Throwable $e) {
                            $currentLineIncipit = false;
                        }

                        foreach($verseSplit['allographic'] as $key => $verse) {
                            $putWhitespace = !$previousLineContinued && $currentAllographicVerse !== '';

                            try {
                                if($putWhitespace) {
                                    $currentVerseLines[count($currentVerseLines) - 1]['length']++;
                                }
                            } catch(\Throwable $e) {
                                // Just for safety
                            }

                            $currentAllographicVerse = $currentAllographicVerse . ($putWhitespace ? ' ' : '') . $verse['value'];

                            $previousLineContinued = $verse['continued'];

                            $absoluteEndVerse = ($isTextAbsolute && (!isset($lines[$lineIndex + 1]) || $lineService->isPageNumber($verse['value']) || $lineService->isPageNumber($lines[$lineIndex + 1]['line']) || $lines[$lineIndex + 1]['coords']['min_y'] - 250 >= $line->min_y));

                            //if($currentLineIncipit || !$previousLineIncipit) {
                                if($line->min_x < $currentVerseMinX)
                                    $currentVerseMinX = $line->min_x;
                                if($line->min_y < $currentVerseMinY)
                                    $currentVerseMinY = $line->min_y;
                                if($line->max_x > $currentVerseMaxX)
                                    $currentVerseMaxX = $line->max_x;
                                if($line->max_y > $currentVerseMaxY)
                                    $currentVerseMaxY = $line->max_y;

                                $currentVerseLines[] = [
                                    'line' => $line,
                                    'offset' => $verseLineOffset,
                                    'length' => $verse['length'],
                                ];
                            //}

                            if($absoluteEndVerse || $verse['isVerseEnd'] || ($line->options()->where('name', '=', Option::explicit)->count() > 0 && $line->value !== NULL)) {
                                if($currentVerseMinX !== 9999999 && $currentVerseMinY !== 9999999 && $currentVerseMaxX !== -1 && $currentVerseMaxY !== -1) {
                                    $currentVerseCoords['minX'][] = trim($currentVerseMinX);
                                    $currentVerseCoords['minY'][] = trim($currentVerseMinY);
                                    $currentVerseCoords['maxX'][] = trim($currentVerseMaxX);
                                    $currentVerseCoords['maxY'][] = trim($currentVerseMaxY);

                                    $currentVerseMinX = 9999999;
                                    $currentVerseMinY = 9999999;
                                    $currentVerseMaxX = -1;
                                    $currentVerseMaxY = -1;
                                }

                                $verseLineOffset = $verseLineOffset + $verse['length'];

                                $v = Verse::create([
                                    'strophe_id' => $currentStrophe?->id,
                                    'text_area_id' => $textArea->id,
                                    'reading_number' => $currentVerseReadingNumber,
                                    'verse_number' => $verse['verseReadingNumber'],
                                    'value' => $currentAllographicVerse,
                                    'min_x' => implode(',', $currentVerseCoords['minX']),
                                    'min_y' => implode(',', $currentVerseCoords['minY']),
                                    'max_x' => implode(',', $currentVerseCoords['maxX']),
                                    'max_y' => implode(',', $currentVerseCoords['maxY']),
                                ]);

                                $deleteCurrentVerse = false;

                                $applyTransformationsService->createVerseTransformations($v, $currentVerseLines);

                                if($copyVerseToPreviousPage && $previousPage !== NULL) {
                                    $previousTextArea = $previousPage->text_areas()->orderBy('id', 'desc')->first();

                                    $verseService->copyVerse(
                                        $v->id,
                                        NULL,
                                        $previousTextArea->id,
                                        $previousPageVerseCoords,
                                    );

                                    $deleteCurrentVerse = $previousTextArea->verses()->count() === 1;

                                    $copyVerseToPreviousPage = false;
                                }

                                if(!$hasStrophes && $v->containsBigInitialLetter()) {
                                    $v->verse_indent = 1;
                                    $v->save();
                                }

                                if($deleteCurrentVerse)
                                    $v->deleteWithForeign();

                                $currentVerseReadingNumber++;
                                $currentAllographicVerse = '';
                                $currentVerseCoords = ['minX' => [], 'minY' => [],'maxX' => [], 'maxY' => [],];
                                $currentVerseLines = [];

                                if($verse['isStropheEnd']) {
                                    $currentStrophe->strophe_number = decodeUnicode($verse['stropheReadingNumber']);
                                    $currentStrophe->save();

                                    if($page->text_areas()->where('text_areas.id', '=', $currentStrophe->text_area_id)->count() === 0) {
                                        // Copy Strophe and verses to next page
                                        $copiedStrophe = $currentStrophe->replicate()->fill([
                                            'text_area_id' => $textArea->id,
                                        ]);

                                        $copiedStrophe->save();

                                        $verseIdsToCopy = $currentStrophe->verses()->pluck('id');

                                        foreach($verseIdsToCopy as $verseIdToCopy) {
                                            $verseService->copyVerse($verseIdToCopy, $copiedStrophe->id);
                                        }

                                        foreach($currentStrophe->verses as $checkVerseIfAlreadyCopied) {
                                            // Here we check to see if we already copied the verse in the previous if statement.
                                            // If so, we transfer the text_area_id to the verse and remove it, since the copied verse has strophe_id NULL
                                            // and thus won't even show up on the page.
                                            $alreadyCopiedVerse = Verse::where('value', '=', $checkVerseIfAlreadyCopied->value)->whereNull('strophe_id')->first();

                                            if($alreadyCopiedVerse) {
                                                $checkVerseIfAlreadyCopied->text_area_id = $alreadyCopiedVerse->text_area_id;
                                                $checkVerseIfAlreadyCopied->max_y = $alreadyCopiedVerse->max_y;
                                                $checkVerseIfAlreadyCopied->min_y = $alreadyCopiedVerse->min_y;
                                                $checkVerseIfAlreadyCopied->max_x = $alreadyCopiedVerse->max_x;
                                                $checkVerseIfAlreadyCopied->min_x = $alreadyCopiedVerse->min_x;
                                                $checkVerseIfAlreadyCopied->save();
                                                $alreadyCopiedVerse->deleteWithForeign();
                                            }
                                        }
                                    }

                                    $currentStrophe = Strophe::create([
                                        'text_area_id' => $textArea->id,
                                        'reading_number' => $currentStropheReadingNumber,
                                    ]);

                                    $currentStropheReadingNumber++;
                                }
                            }
                        }

                        $previousLineIncipit = $currentLineIncipit;
                    }

                    if($currentVerseMinX !== 9999999 && $currentVerseMinY !== 9999999 && $currentVerseMaxX !== -1 && $currentVerseMaxY !== -1) {
                        $currentVerseCoords['minX'][] = trim($currentVerseMinX);
                        $currentVerseCoords['minY'][] = trim($currentVerseMinY);
                        $currentVerseCoords['maxX'][] = trim($currentVerseMaxX);
                        $currentVerseCoords['maxY'][] = trim($currentVerseMaxY);

                        $currentVerseMinX = 9999999;
                        $currentVerseMinY = 9999999;
                        $currentVerseMaxX = -1;
                        $currentVerseMaxY = -1;
                    }
                }

                // To make sure that rectangles are not drawn for the previous page
                $previousPageVerseCoords = $currentVerseCoords;
                $currentVerseCoords = ['minX' => [], 'minY' => [],'maxX' => [], 'maxY' => [],];

                $previousPage = $page;

                $copyVerseToPreviousPage = !$isTextAbsolute && $currentAllographicVerse !== '';
            }

            $previousChapter = $chapter;
        }

        $take = 500;
        $skip = 0;

        do {
            $allVerses = Verse::skip($skip)->take($take)->get();

            $skip += $take;

            if (!$allVerses->isEmpty()) {
                foreach($allVerses as $verseToSearchString) {
                    $specialVerseNumber = NULL;

                    if($verseToSearchString->transformations()->where('name', '=', 'incipit')->count() > 0) {
                        $specialVerseNumber = 'Incipit';

                        if($verseToSearchString->transformations()->where('name', '=', 'aventiure')->count()  > 0) {
                            $transformation = $verseToSearchString
                                ->transformations()
                                ->select('transformations.id as id')
                                ->firstWhere('transformations.name', '=', 'aventiure');

                            $transformationValue = TransformationValue::where('transformation_id', '=', $id = $transformation->id)
                                ->where('key', '=', 'aventiureValue')->first();

                            if($transformationValue) {
                                $specialVerseNumber = 'Incipit <i>âventiure</i> ' . $transformationValue->value;
                            }
                        }
                    }

                    SearchString::create([
                        'chapter_id' => $verseToSearchString->text_area->page->chapter_id,
                        'page_id' => $verseToSearchString->text_area->page_id,
                        'verse_id' => $verseToSearchString->id,
                        'chapter_name' => $chapterService->chapterNameWithoutArticles($verseToSearchString->text_area->page->chapter),
                        'strophe_number' => $specialVerseNumber !== NULL ? NULL : $verseToSearchString->strophe?->strophe_number,
                        'strophe_reading_number' => $verseToSearchString->strophe?->reading_number,
                        'verse_number' => $specialVerseNumber ?? $verseToSearchString->verse_number,
                        'verse_reading_number' => $verseToSearchString->reading_number,
                        'value' => trim($transkribusService->removePunctures($verseToSearchString->value)),
                        'normalized_value' => trim($verseService->replaceAbbreviations($transkribusService->removePunctures($transkribusService->normalizeString($verseToSearchString->value)), $verseToSearchString)),
                    ]);
                }
            }
        } while(!$allVerses->isEmpty());

        $exceptionCases = [
            MoveBigInitialPageNinteen::class,
            MoveFirstIncipitOfTabula::class,
            RemovePageSeventeen::class,
            RemoveTextOnPageEighteen::class,
            RemoveLinesFromIncipitStropheInNibelungenlied::class,
            RenameKudrunSearchTermExplicit::class,
            UpdateVerseNumberPageTwoHundredTwentyFour::class,
            AddAmenToVersePageFourHundredEightyFour::class,
        ];

        foreach ($exceptionCases as $exceptionCase) {
            $case = app()->make($exceptionCase);

            try {
                $case->__invoke();
            } catch (ExceptionCaseCouldNotBeAppliedException $e) {
                Log::error($e->getMessage());
            }
        }
    }
}
