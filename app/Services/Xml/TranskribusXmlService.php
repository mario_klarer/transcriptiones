<?php

namespace App\Services\Xml;

use App\Models\Option;
use App\Services\Transkribus\TranskribusService;
use Illuminate\Support\Facades\Log;

class TranskribusXmlService {
    private TranskribusService $transkribusService;

    public function __construct(TranskribusService $transkribusService)
    {
        $this->transkribusService = $transkribusService;
    }

    public function getFolio($xml): string {
        $folio = "MVT";

        try {
            $found = false;
            foreach($xml->Page->TextRegion as $textRegion) {
                foreach($textRegion->TextLine as $line) {
                    $opts = $this->transkribusService->parseOptionString($line->attributes()->custom);

                    if(isset($opts['folio'])) {
                        $folio = $opts['folio'][0]['value'] . $opts['folio'][0]['side'];
                        $found = true;
                        break;
                    }
                }

                if($found)
                    break;
            }
        } catch (\Throwable $e) {
            // Oof
            Log::error($e->getMessage());
        }

        return $folio;
    }

    public function getTextRegions($xml): array {
        $regions = [];

        try {
            foreach($xml->Page->TextRegion as $textRegion) {
                $options = $this->getOptions($textRegion);

                if(in_array($options[Option::readingOrder][0]['index'], ["0", "1", "2"])) {
                    $regions[(int) $options[Option::readingOrder][0]['index']] = $textRegion;
                }
            }
        } catch (\Throwable $e) {
            // Oof
            Log::error($e->getMessage());
        }

        return $regions;
    }

    public function getLinesWithOptionsAndCoordsFromArea($xmlArea): array {
        $lines = [];

        foreach($xmlArea->TextLine as $xmlLine) {
            $options = $this->getOptions($xmlLine);
            $coords = $this->getCoords($xmlLine);
            $line = $xmlLine->TextEquiv->Unicode;

            try {
                $readingNumber = $options[Option::readingOrder][0]['index'];
            } catch (\Throwable $e) {
                $readingNumber = 999;
            }

            $lines[] = [
                'options' => $options,
                'coords' => $coords,
                'line' => $line,
                'readingNumber' => $readingNumber,
            ];
        }

        return $lines;
    }

    public function getOptions($xmlElement): array {
        return $this->transkribusService->parseOptionString($xmlElement->attributes()->custom);
    }

    public function getCoords($xmlElement): array {
        return $this->transkribusService->parseCoordinateString($xmlElement->Coords->attributes()->points);
    }

    public function getImageSize($xml): array {
        return [
            'width' => (int) $xml->Page->attributes()->imageWidth,
            'height' => (int) $xml->Page->attributes()->imageHeight,
        ];
    }
}
