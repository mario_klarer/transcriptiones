<?php

namespace App\Services\Chapter;

use App\Models\Chapter;

class ChapterService {
    public function createChapter(string $name, Chapter $previousChapter = NULL, bool $hasStrophes, bool $isTextAbsolute): Chapter {
        return Chapter::create([
            'name' => $name,
            'previous_chapter_id' => $previousChapter?->id,
            'has_strophes' => $hasStrophes,
            'is_absolute' => $isTextAbsolute,
        ]);
    }

    public function addNextChapter(Chapter $chapter, Chapter $nextChapter): Chapter {
        $chapter->next_chapter_id = $nextChapter->id;
        return $chapter;
    }

    public function chapterNameWithoutArticles(Chapter $chapter): string {
        $name = $chapter->name;

        $replaceStrings = ['Der ', 'Die ', 'Das '];

        foreach($replaceStrings as $replaceString) {
            $name = str_replace($replaceString, '', $name);
        }

        return ucfirst($name);
    }
}
