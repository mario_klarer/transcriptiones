<?php

namespace App\Services\Option;

use App\Models\Option;
use App\Models\OptionValue;
use Illuminate\Database\Eloquent\Model;

class OptionService {
    public function createOption(string $name, Model $optionable, array $optionValues): Option {
        $option = Option::create([
            'name' => $name,
            'optionable_type' => $optionable->getMorphClass(),
            'optionable_id' => $optionable->id,
        ]);

        foreach($optionValues as $key => $value) {
            $this->createOptionValue($option, $key, $value);
        }

        return $option;
    }

    public function createOptionValue(Option $option, string $key, string $value) {
        return OptionValue::create([
            'option_id' => $option->id,
            'key' => $key,
            'value' => $value,
        ]);
    }
}
