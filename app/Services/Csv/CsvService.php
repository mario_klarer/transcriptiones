<?php

namespace App\Services\Csv;

class CsvService {
    public function csvToArray(string $path): array {
        $row = 0;

        $ret = [];
        $header = [];

        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if($row === 0) {
                    foreach($data as $i => $temp) {
                        $data[$i] = str_replace("\xEF\xBB\xBF", "", $temp);
                    }

                    $header = $data;

                    $row++;

                    continue;
                }

                foreach($header as $key => $headline) {
                    $ret[$row - 1][$headline] = $data[$key];
                }

                $row++;
            }
            fclose($handle);
        }

        return $ret;
    }
}
