<?php

namespace App\Services\Page;

use App\Models\Chapter;
use App\Models\Page;

class PageService {
    public function createPage(Chapter $chapter, ?Page $previousPage, string $folio, int $fileNumber, array $imageSize): Page {
        return Page::create([
            'chapter_id' => $chapter->id,
            'previous_page_id' => $previousPage?->id,
            'folio' => $folio,
            'page_number' => $fileNumber,
            'image_height' => $imageSize['height'],
            'image_width' => $imageSize['width'],
        ]);
    }

    public function addNextPage(Page $page, Page $nextPage): Page {
        $page->next_page_id = $nextPage->id;
        return $page;
    }
}
