<?php

namespace App\Services\Line;

use App\Models\Line;
use App\Models\TextArea;

class LineService {
    public function createLine(TextArea $textArea, int $readingNumber, array $coords, ?string $value, int $emptyLinesCount, ?int $lineIndent, ?bool $smallIndent = false, bool $isTextAbsolute = false): Line {
        return Line::create([
            'reading_number' => $readingNumber,
            'line_number' => $value === NULL || $value === '' || ($isTextAbsolute && $this->isPageNumber($value)) ? NULL : ($readingNumber - $emptyLinesCount + 1),
            'text_area_id' => $textArea->id,
            'value' => $value,
            'line_indent' => $lineIndent,
            'small_indent' => $smallIndent,
            'min_x' => $coords['min_x'],
            'max_x' => $coords['max_x'],
            'min_y' => $coords['min_y'],
            'max_y' => $coords['max_y'],
        ]);
    }

    public function isPageNumber(string $value): bool {
        return preg_match('/^(·)?\s*[A-Z]*\s*(·|)?$/', $value);
    }
}
