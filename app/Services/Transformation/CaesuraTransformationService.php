<?php

namespace App\Services\Transformation;

use App\Exceptions\CrossingTransformationsException;
use App\Models\Line;
use App\Models\Option;
use App\Models\Transformation;
use App\Models\Verse;
use App\Services\Transformation\Traits\ParsesEnclosingOption;

class CaesuraTransformationService extends TransformationService {
    use ParsesEnclosingOption;

    public function createTransformation(Line $line): ?Transformation {
        return NULL;
    }

    public function createVerseTransformation(Verse $verse, array $lines): ?Transformation {
        $this->createEnclosingVerseTransformations($verse, $lines, Option::caesura, Transformation::caesura, true, [], true);

        return NULL;
    }
}
