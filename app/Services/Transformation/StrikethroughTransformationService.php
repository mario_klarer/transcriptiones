<?php

namespace App\Services\Transformation;

use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;
use App\Services\Transformation\Traits\ChecksForThinInitial;
use App\Services\Transformation\Traits\ParsesEnclosingOption;

class StrikethroughTransformationService extends TransformationService {
    use ParsesEnclosingOption;
    public function createTransformation(Line $line): ?Transformation {
        $this->createEnclosingLineTransformations($line, true, Option::strikethrough, Transformation::strikethrough);

        return NULL;
    }

    public function createVerseTransformation(Verse $verse, array $lines): ?Transformation {
        $this->createEnclosingVerseTransformations($verse, $lines, Option::strikethrough, Transformation::strikethrough, true);

        return NULL;
    }
}
