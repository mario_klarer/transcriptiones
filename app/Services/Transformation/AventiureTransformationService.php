<?php

namespace App\Services\Transformation;

use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;

class AventiureTransformationService extends TransformationService {
    private const specialAventiurePages = [207, 223, 224, 295, 315, 370, 409, 427, 428, 442];

    public function createTransformation(Line $line): ?Transformation {
        $option = $line->options()->where('name', '=', Option::aventiure)->first();

        if(!$option) {
            return NULL;
        }

        $junicode = $line->value;
        $start = 0;
        $end = count(splitJunicode($junicode));

        $transformation = Transformation::create([
            'name' => Transformation::aventiure,
            'start' => $start,
            'end' => $end,
            'order' => 2,   // Incipit ist auf 1
            'normalized' => false,
            'transformable_type' => Line::class,
            'transformable_id' => $line->id
        ]);

        $value = $option->option_values()->where('key', '=', OptionValue::value)->first()->value;

        TransformationValue::create([
            'key' => TransformationValue::aventiureValue,
            'value' => $value,
            'transformation_id' => $transformation->id
        ]);

        if(in_array($line->text_area->page->page_number, self::specialAventiurePages))
            TransformationValue::create([
                'key' => TransformationValue::specialAventiure,
                'value' => true,
                'transformation_id' => $transformation->id
            ]);

        return $transformation;
    }

    public function createVerseTransformation(Verse $verse, array $lines): ?Transformation {
        if(count($lines) <= 0)
            return NULL;

        // For aventiures the verse can only consist of a single line
        $line = $lines[0];

        $option = $line['line']->options()->where('name', '=', Option::aventiure)->first();

        if(!$option) {
            return NULL;
        }

        $junicode = $verse->value;
        $start = 0;
        $end = count(splitJunicode($junicode));

        $transformation =  Transformation::create([
            'name' => Transformation::aventiure,
            'start' => $start,
            'end' => $end,
            'order' => 2,
            'normalized' => false,
            'transformable_type' => Verse::class,
            'transformable_id' => $verse->id,
        ]);

        $value = $option->option_values()->where('key', '=', OptionValue::value)->first()->value;

        TransformationValue::create([
            'key' => TransformationValue::aventiureValue,
            'value' => $value,
            'transformation_id' => $transformation->id
        ]);

        if(in_array($verse->text_area->page->page_number, self::specialAventiurePages))
            TransformationValue::create([
                'key' => TransformationValue::specialAventiure,
                'value' => true,
                'transformation_id' => $transformation->id
            ]);

        return $transformation;
    }
}
