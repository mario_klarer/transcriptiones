<?php

namespace App\Services\Transformation;

use App\Models\Line;
use App\Models\Verse;

class ApplyTransformationsService {
    private array $services;
    private array $verseServices;

    public function __construct(IncipitTransformationService $incipitTransformationService,
                                CaesuraTransformationService $caesuraTransformationController,
                                RedInitialTransformationService $redInitialTransformationService,
                                BlueInitialTransformationService $blueInitialTransformationService,
                                RedRubricTransformationService $redRubricTransformationService,
                                BigInitialTransformationService $bigInitialTransformationService,
                                MissingInitialTransformationService $missingInitialTransformationService,
                                AbbrevTransformationService $abbrevTransformationService,
                                StrikethroughTransformationService $strikethroughTransformationService,
                                AmenTransformationService $amenTransformationService,
                                InsertionTransformationService $insertionTransformationService,
                                AventiureTransformationService $aventiureTransformationService,)
    {
        $this->services = [
            $incipitTransformationService,
            $redInitialTransformationService,
            $blueInitialTransformationService,
            $redRubricTransformationService,
            $bigInitialTransformationService,
            $missingInitialTransformationService,
            $abbrevTransformationService,
            $strikethroughTransformationService,
            $aventiureTransformationService,
            $amenTransformationService,
            $insertionTransformationService,
        ];

        $this->verseServices = [
            $incipitTransformationService,
            $caesuraTransformationController,
            $redInitialTransformationService,
            $blueInitialTransformationService,
            $redRubricTransformationService,
            $bigInitialTransformationService,
            $missingInitialTransformationService,
            $abbrevTransformationService,
            $strikethroughTransformationService,
            $aventiureTransformationService,
            $amenTransformationService,
            $insertionTransformationService,
        ];
    }

    public function createTransformations(Line $line) {
        foreach($this->services as $service) {
            $service->createTransformation($line);
        }
    }

    public function createVerseTransformations(Verse $verse, array $lines) {
        foreach($this->verseServices as $service) {
            $service->createVerseTransformation($verse, $lines);
        }
    }
}
