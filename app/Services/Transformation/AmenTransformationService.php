<?php

namespace App\Services\Transformation;

use App\Models\Line;
use App\Models\Option;
use App\Models\Transformation;
use App\Models\Verse;
use App\Services\Transformation\Traits\ParsesEnclosingOption;

class AmenTransformationService extends TransformationService {
    use ParsesEnclosingOption;
    public function createTransformation(Line $line): ?Transformation {
        return NULL;
    }

    public function createVerseTransformation(Verse $verse, array $lines): ?Transformation {
        $hasAmen = false;

        foreach($lines as $line) {
            $hasAmen = $line['line']->options()->where('name', '=', Option::amen)->count() > 0;

            if($hasAmen)
                break;
        }

        if(!$hasAmen)
            return NULL;

        $junicode = $verse->value;
        $start = 0;
        $end = count(splitJunicode($junicode));

        $create = [
            'name' => Transformation::amen,
            'start' => $start,
            'end' => $end,
            'order' => 99999,
            'normalized' => false,
            'transformable_type' => Verse::class,
            'transformable_id' => $verse->id,
        ];

        $transformation = Transformation::create($create);
        $create['normalized'] = true;
        $normalizedTransformation = Transformation::create($create);

        return $transformation;
    }
}
