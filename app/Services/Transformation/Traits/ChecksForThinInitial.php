<?php


namespace App\Services\Transformation\Traits;

use App\Exceptions\CrossingTransformationsException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;
use Illuminate\Support\Facades\Log;

trait ChecksForThinInitial {
    protected function checkAndCreateThinTransformationValue(Line $line): void {
        $transformation = $line->transformations()->where('name', '=', Transformation::bigInitial)->first();

        if($transformation
            && $transformation->transformation_values()
                ->where('name', '=', TransformationValue::offsetText)
                ->where('value', '=', 0)
            && (str_starts_with($line->value, 'I')
                || str_starts_with($line->value, 'J'))) {
            TransformationValue::create([
                'key' => TransformationValue::isThinInitial,
                'value' => 'is-thin-initial',
                'transformation_id' => $transformation->id,
            ]);
        }
    }
}
