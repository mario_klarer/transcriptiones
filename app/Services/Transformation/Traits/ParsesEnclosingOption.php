<?php


namespace App\Services\Transformation\Traits;

use App\Exceptions\CrossingTransformationsException;
use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;
use Illuminate\Support\Facades\Log;

trait ParsesEnclosingOption {
    protected function createEnclosingLineTransformations(Line $line, bool $includeNormalized, string $optionName, string $transformationName, array $addValues = [], bool $skipAllographic = false) {
        $options = $line->options()->where('name', '=', $optionName)->get();

        foreach($options as $option) {
            $valueOffset = $option->option_values()->where('key', '=', OptionValue::offset)->first();
            $valueLength = $option->option_values()->where('key', '=', OptionValue::length)->first();

            if (!$valueOffset || !$valueLength)
                continue;

            $start = $valueOffset->value;
            $end = $valueOffset->value + $valueLength->value;

            $order = $this->recalculateOrder($line, $start, $end);

            $create = [
                'name' => $transformationName,
                'start' => $start,
                'end' => $end,
                'order' => $order,
                'normalized' => false,
                'transformable_type' => Line::class,
                'transformable_id' => $line->id,
            ];

            if(!$skipAllographic) {
                $transformation = Transformation::create($create);

                $this->addValuesToTransformation($transformation, $option, $addValues);
            }

            if($includeNormalized) {
                $create['normalized'] = true;

                $normalizedTransformation = Transformation::create($create);

                $this->addValuesToTransformation($normalizedTransformation, $option, $addValues);
            }
        }

        return NULL;
    }

    protected function createEnclosingVerseTransformations(Verse $transformable, array $lines, string $optionName, string $transformationName, bool $includeNormalized, array $addValues = [], bool $skipAllographic = false) {
        $verseOffset = 0;

        foreach($lines as $line) {
            $verseOffsetBefore = $verseOffset;
            $verseOffset = $verseOffset + $line['length'];

            $options = $line['line']->options()->where('name', '=', $optionName)->get();

            foreach($options as $option) {
                $valueOffset = $option->option_values()->where('key', '=', OptionValue::offset)->first();
                $valueLength = $option->option_values()->where('key', '=', OptionValue::length)->first();

                if(!$valueOffset || !$valueLength) {
                    continue;
                }

                $valueOffset = $valueOffset->value;
                $valueLength = $valueLength->value;

                if($valueOffset < $line['offset'] || ($valueOffset + $valueLength) > ($line['offset'] + $line['length'])) {
                    continue;
                }

                $start = $verseOffsetBefore + ($valueOffset - $line['offset']);
                $end = $verseOffsetBefore + ($valueOffset - $line['offset']) + $valueLength;

                try {
                    $order = $this->recalculateOrder($transformable, $start, $end);
                    $create = [
                        'name' => $transformationName,
                        'start' => $start,
                        'end' => $end,
                        'order' => $order,
                        'normalized' => false,
                        'transformable_type' => Verse::class,
                        'transformable_id' => $transformable->id,
                    ];

                    if(!$skipAllographic) {
                        $transformation = Transformation::create($create);

                        $this->addValuesToTransformation($transformation, $option, $addValues);
                    }

                    if(!$includeNormalized)
                        continue;

                    $create['normalized'] = true;

                    $normalizedTransformation = Transformation::create($create);

                    $this->addValuesToTransformation($normalizedTransformation, $option, $addValues);
                } catch (CrossingTransformationsException $e) {
                    // Do nothing
                    Log::debug('Crossing Transformations Exception');
                }
            }
        }
    }

    private function addValuesToTransformation(Transformation $transformation, Option $option, array $values) {
        foreach($values as $value) {
            $optionValue = $option->option_values()->where('key', '=', $value['optionValueKey'])->first();

            if(!$optionValue)
                continue;

            TransformationValue::create([
                'key' => $value['transformationValueKey'],
                'value' => $optionValue->value,
                'transformation_id' => $transformation->id,
            ]);
        }
    }

    abstract protected function recalculateOrder(Line|Verse $transformable, int $start, int $end): int;
}
