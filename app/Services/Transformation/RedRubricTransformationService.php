<?php

namespace App\Services\Transformation;

use App\Models\Line;
use App\Models\Option;
use App\Models\Transformation;
use App\Models\Verse;
use App\Services\Transformation\Traits\ParsesEnclosingOption;

class RedRubricTransformationService extends TransformationService {
    use ParsesEnclosingOption;

    public function createTransformation(Line $line): ?Transformation {
        $this->createEnclosingLineTransformations($line, true, Option::redRubric, Transformation::redRubric);

        return NULL;
    }

    public function createVerseTransformation(Verse $verse, array $lines): ?Transformation {
        $this->createEnclosingVerseTransformations($verse, $lines, Option::redRubric, Transformation::redRubric, true);

        return NULL;
    }
}
