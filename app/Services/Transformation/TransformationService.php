<?php

namespace App\Services\Transformation;

use App\Exceptions\CrossingTransformationsException;
use App\Models\Line;
use App\Models\Transformation;
use App\Models\Verse;

abstract class TransformationService {
    public function createTransformation(Line $line): ?Transformation {
        return NULL;
    }

    public function createVerseTransformation(Verse $verse, array $lines): ?Transformation {
        return NULL;
    }

    protected function recalculateOrder(Line|Verse $transformable, int $start, int $end): int {
        $transformations = $transformable->transformations()->orderBy('order', 'ASC')->get();

        $order = 1;

        foreach($transformations as $transformation) {
            throw_if(($transformation->start < $start && $transformation->end < $end && $transformation->end > $start)
                || ($start < $transformation->start && $end < $transformation->end && $end > $transformation->start), CrossingTransformationsException::class);

            if($end === $transformation->end && $start > $transformation->start) {
                $order = $transformation->order;
            } else if($end >= $transformation->end)
                $order = $transformation->order + 1;
        }

        $increaseTransformations = $transformable->transformations()->where('order', '>=', $order);

        foreach($increaseTransformations as $increase) {
            $increase->order = $increase->order + 1;
            $increase->save();
        }

        return $order;
    }
}
