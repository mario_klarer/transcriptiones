<?php

namespace App\Services\Transformation;

use App\Models\Line;
use App\Models\Option;
use App\Models\Transformation;
use App\Models\Verse;

class IncipitTransformationService extends TransformationService {
    public function createTransformation(Line $line): ?Transformation {
        $option = $line->options()->where(function ($q) {
            $q->where('name', '=', Option::incipit)->orWhere('name', '=', Option::explicit);
        })->first();

        if(!$option) {
            return NULL;
        }

        $junicode = $line->value;
        $start = 0;
        $end = count(splitJunicode($junicode));

        Transformation::create([
            'name' => Transformation::incipit,
            'start' => $start,
            'end' => $end,
            'order' => 1,
            'normalized' => true,
            'transformable_type' => Line::class,
            'transformable_id' => $line->id
        ]);

        return Transformation::create([
            'name' => Transformation::incipit,
            'start' => $start,
            'end' => $end,
            'order' => 1,
            'normalized' => false,
            'transformable_type' => Line::class,
            'transformable_id' => $line->id
        ]);
    }

    public function createVerseTransformation(Verse $verse, array $lines): ?Transformation {
        if(count($lines) <= 0)
            return NULL;

        // For incipits the verse can only consist of a single line
        $line = $lines[0];

        $option = $line['line']->options()->where(function ($q) {
            $q->where('name', '=', Option::incipit)->orWhere('name', '=', Option::explicit);
        })->first();

        if(!$option) {
            return NULL;
        }

        $junicode = $verse->value;
        $start = 0;
        $end = count(splitJunicode($junicode));

        Transformation::create([
            'name' => Transformation::incipit,
            'start' => $start,
            'end' => $end,
            'order' => 1,
            'normalized' => true,
            'transformable_type' => Verse::class,
            'transformable_id' => $verse->id
        ]);

        return Transformation::create([
            'name' => Transformation::incipit,
            'start' => $start,
            'end' => $end,
            'order' => 1,
            'normalized' => false,
            'transformable_type' => Verse::class,
            'transformable_id' => $verse->id
        ]);
    }
}
