<?php

namespace App\Services\Transformation;

use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;
use App\Services\Transformation\Traits\ParsesEnclosingOption;

class InsertionTransformationService extends TransformationService {
    use ParsesEnclosingOption;

    public function createTransformation(Line $line): ?Transformation {
        return NULL;
    }

    public function createVerseTransformation(Verse $verse, array $lines): ?Transformation {
        $this->createEnclosingVerseTransformations($verse, $lines, Option::insertion, Transformation::insertion, true, [['optionValueKey' => OptionValue::insert, 'transformationValueKey' => TransformationValue::insert]], true);

        return NULL;
    }
}
