<?php

namespace App\Services\Transformation;

use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Transformation;
use App\Models\TransformationValue;
use App\Models\Verse;
use App\Services\Transformation\Traits\ChecksForThinInitial;
use App\Services\Transformation\Traits\ParsesEnclosingOption;

class BigInitialTransformationService extends TransformationService {
    use ParsesEnclosingOption, ChecksForThinInitial;

    public function createTransformation(Line $line): ?Transformation {
        $this->createEnclosingLineTransformations($line, true, Option::bigInitial, Transformation::bigInitial, [['optionValueKey' => OptionValue::height, 'transformationValueKey' => TransformationValue::height], ['optionValueKey' => OptionValue::offsetText, 'transformationValueKey' => TransformationValue::offsetText]]);

        $this->checkAndCreateThinTransformationValue($line);

        return NULL;
    }

    public function createVerseTransformation(Verse $verse, array $lines): ?Transformation {
        $this->createEnclosingVerseTransformations($verse, $lines, Option::bigInitial, Transformation::bigInitial, true);

        return NULL;
    }
}
