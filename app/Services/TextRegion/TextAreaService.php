<?php

namespace App\Services\TextRegion;

use App\Models\Page;
use App\Models\TextArea;

class TextAreaService {
    public function createTextArea(Page $page, int $readingNumber, array $coords): TextArea {
        return TextArea::create([
            'page_id' => $page->id,
            'reading_number' => $readingNumber,
            'min_x' => $coords['min_x'],
            'min_y' => $coords['min_y'],
            'max_x' => $coords['max_x'],
            'max_y' => $coords['max_y'],
        ]);
    }
}
