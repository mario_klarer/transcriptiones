<?php

namespace App\Services\Transkribus;

use App\Models\Option;

class TranskribusService {
    public function parseOptionString(string $optionString): array {
        $options = str_split(trim($optionString));

        $attributes = array();
        $attr = "";
        $current_opt = "";
        $current_opt_value = "";
        $opt = array();
        $is_attribute_name = true;
        $is_opt_name = true;

        foreach($options as $char) {
            if($char != ' ') {
                if($is_attribute_name) {
                    if($char != '{'){
                        $attr = $attr . $char;
                    } else {
                        $is_attribute_name = false;
                    }
                } else {
                    if($is_opt_name) {
                        if($char == '}') {
                            $is_attribute_name = true;

                            if(isset($attributes[$attr])) {
                                $attributes[$attr][sizeof($attributes[$attr])] = $opt;
                            } else {
                                $attributes[$attr][0] = $opt;
                            }

                            $attr = "";
                            $opt = array();
                        } else if($char != ':') {
                            $current_opt = $current_opt . $char;
                        } else {
                            $is_opt_name = false;
                        }
                    } else {
                        if($char != ';') {
                            $current_opt_value = $current_opt_value . $char;
                        } else {
                            $opt[$current_opt] = $current_opt_value;
                            $is_opt_name = true;

                            $current_opt = "";
                            $current_opt_value = "";
                        }
                    }
                }
            }
        }

        return $attributes;
    }

    public function parseCoordinateString(string $coordinateString): array {
        if(strlen($coordinateString) <= 0){
            return [];
        }

        $xyStrings = explode(" ", $coordinateString);

        $xValues = [];
        $yValues = [];

        foreach($xyStrings as $xystring){
            $temp = explode(",", $xystring);
            $xValues[] = $temp[0];
            $yValues[] = $temp[1];
        }

        $max_x = $xValues[0];
        $min_x = $xValues[0];

        $max_y = $yValues[0];
        $min_y = $yValues[0];

        for($i = 1; $i < sizeof($xValues); $i++) {
            if($max_x < $xValues[$i]) {
                $max_x = $xValues[$i];
            }

            if($max_y < $yValues[$i]) {
                $max_y = $yValues[$i];
            }

            if($min_x > $xValues[$i]) {
                $min_x = $xValues[$i];
            }

            if($min_y > $yValues[$i]) {
                $min_y = $yValues[$i];
            }
        }

        return [
            "max_x" => $max_x,
            "min_x" => $min_x,
            "max_y" => $max_y,
            "min_y" => $min_y
        ];
    }

    public function normalizeString(string $string): string {
        $toReplace = [
            "∂",
            "ɧ",
            "ɱ",
            "ŋ",
            "ꝛ",
            "ɞ",
            "σ",
            "ſ",
            "ꜩ",
            "ʒ",
            "",
            "ỽ",
            "ⱳ",
            "ͦ",
            "̆",
            "ÿ",
            "Ÿ",
            "Yᷓ",
            "Ѣᷓ",
            "ä",
            "Ä",
            "ö",
            "Ö",
            "ü",
            "Ü",
            "ë",
            "Ë",
            "yᷓ",
            "yᷓ",
            "aᷓ",
            "Aᷓ",
            "eᷓ",
            "Eᷓ",
            "oᷓ",
            "Oᷓ",
            "euᷓ",
            "Euᷓ",
            "uᷓ",
            "Uᷓ",
            "vᷓ",
            "Vᷓ",
            "wᷓ",
            "Wᷓ",
            "̄",
            "ˀ",
        ];
        $replacements = [
            "d",
            "h",
            "m",
            "n",
            "r",
            "s",
            "s",
            "s",
            "tz",
            "z",
            "s",
            "v",
            "w",
            "o",
            "",
            "y",
            "Y",
            "Y",
            "Ѣϸ",
            "ä",
            "Ä",
            "ö",
            "Ö",
            "ü",
            "Ü",
            "e",
            "E",
            "y",
            "Y",
            "ä",
            "Ä",
            "e",
            "E",
            "ö",
            "Ö",
            "eu",
            "Eu",
            "ü",
            "Ü",
            "v̈",
            "V̈",
            "w",
            "w",
            "",
            "",
        ];

        foreach($toReplace as $key => $replace) {
            $string = str_replace($replace, $replacements[$key], $string);
        }

        return $string;
    }

    public function removePunctures(string $string): string {
        $toReplace = ['⸗', '~', '·', ':', ''];
        $replacements = ['', '', '', '', ''];

        foreach($toReplace as $key => $replace) {
            $string = str_replace(' ' . $replace, $replacements[$key], $string);
            $string = str_replace($replace, $replacements[$key], $string);
        }

        return $string;
    }

    public function stringSplitJunicode(string $junicode): array {
        return preg_split('//u', $junicode, null, PREG_SPLIT_NO_EMPTY);
    }

    public function getFileNameFromFileNumber(int $number, string $extension = "xml"): string {
        return substr("00000000" . $number, -8) . '.' . $extension;
    }

    public function loadXml(int $fileNumber) {
        return simplexml_load_file(resource_path('/xml/pages/' . $this->getFileNameFromFileNumber($fileNumber)));
    }
}
