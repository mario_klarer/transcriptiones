<?php

namespace App\Services\Verse;

use App\Models\Line;
use App\Models\Option;
use App\Models\OptionValue;
use App\Models\Verse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class VerseService {
    function splitLineIntoVerseParts(Line $line, bool $previousLineIncipit): array {
        $verseOptions = $line->options()->where('name', '=', Option::endOfVerse)->get();
        $stropheOptions = $line->options()->where('name', '=', Option::endOfStrophe)->get();
        $hasContinuedOption = $line->options()->where('name', '=', Option::hyphenation)->whereHas('option_values', function($q) {
            $q->where('key', '=', OptionValue::continued)->where('value', '=', 'true');
        })->whereHas('option_values', function($q) {
            $q->where('key', '=', OptionValue::offset)->where('value', '<>', '0');
        })->exists();

        $lineValue = $line->value;

        if($line->options()->where('name', '=', Option::incipit)->count() > 0 || $line->options()->where('name', '=', Option::explicit)->count() > 0) {
            $ret = ['allographic' => [
                [
                    'value' => $this->replaceEndingCharacters($lineValue),
                    'isVerseEnd' => false,
                    'verseReadingNumber' => '',
                    'isStropheEnd' => false,
                    'stropheReadingNumber' => '',
                    'continued' => false,
                    'incipit' => true,
                    'length' => count(splitJunicode($lineValue)),
                ]
            ], 'normalized' => [
                [
                    'value' => $this->replaceEndingCharacters($lineValue),
                    'isVerseEnd' => false,
                    'verseReadingNumber' => '',
                    'isStropheEnd' => false,
                    'stropheReadingNumber' => '',
                    'continued' => false,
                    'incipit' => true,
                    'length' => count(splitJunicode($lineValue)),
                ]
            ]];
        } else {
            $ret = ['allographic' => $this->splitStringIntoVerseParts($lineValue, $verseOptions, $stropheOptions, $hasContinuedOption, $previousLineIncipit), 'normalized' => $this->splitStringIntoVerseParts($lineValue, $verseOptions, $stropheOptions, $hasContinuedOption, $previousLineIncipit)];
        }

        return $ret;
    }

    public function copyVerse(int $verseIdToCopy, ?int $stropheId = NULL, ?int $textAreaId = NULL, ?array $coords = NULL) {
        $verseToCopy = Verse::find($verseIdToCopy);

        $copiedVerse = $verseToCopy->replicate()->fill([
            'strophe_id' => $stropheId,
        ]);

        if($textAreaId !== NULL) {
            $copiedVerse->text_area_id = $textAreaId;
        }

        if($coords !== NULL) {
            $copiedVerse->min_x = implode(',', $coords['minX']);
            $copiedVerse->min_y = implode(',', $coords['minY']);
            $copiedVerse->max_x = implode(',', $coords['maxX']);
            $copiedVerse->max_y = implode(',', $coords['maxY']);
        }

        $copiedVerse->save();

        $optionsToCopy = $verseToCopy->options;

        foreach($optionsToCopy as $optionToCopy) {
            $optionValuesToCopy = $optionToCopy->option_values;

            $copiedOption = $optionToCopy->replicate()->fill([
                'optionable_id' => $copiedVerse->id,
            ]);

            $copiedOption->save();

            foreach($optionValuesToCopy as $optionValueToCopy) {
                $copiedOptionValue = $optionValueToCopy->replicate()->fill([
                    'option_id' => $copiedOption->id,
                ]);

                $copiedOptionValue->save();
            }
        }

        $transformationsToCopy = $verseToCopy->transformations;

        foreach($transformationsToCopy as $transformationToCopy) {
            $transformationValuesToCopy = $transformationToCopy->transformation_values;

            $copiedTransformation = $transformationToCopy->replicate()->fill([
                'transformable_id' => $copiedVerse->id,
            ]);

            $copiedTransformation->save();

            foreach($transformationValuesToCopy as $transformationValueToCopy) {
                $copiedTransformationValue = $transformationValueToCopy->replicate()->fill([
                    'transformation_id' => $copiedTransformation->id,
                ]);

                $copiedTransformationValue->save();
            }
        }
    }

    private function splitStringIntoVerseParts(?string $lineValue, Collection $verseOptions, Collection $stropheOptions, bool $hasContinuedOption, bool $previousLineIncipit): array {
        $ret = [];

        if($previousLineIncipit) {
            $ret[] = [
                'value' => '',
                'isVerseEnd' => true,
                'verseReadingNumber' => '',
                'isStropheEnd' => false,
                'stropheReadingNumber' => '',
                'continued' => false,
                'incipit' => false,
                'length' => 0,
            ];
        }

        $currentVerse = '';
        foreach(splitJunicode($lineValue) as $key => $character) {
            $currentVerse .= $character;

            foreach($verseOptions as $verseOption) {
                $verseEnd = $verseOption->option_values()->where('key', '=', OptionValue::offset)->first()->value;
                if($key == $verseEnd) {
                    $isStropheEnd = false;
                    $stropheReadingNumber = '';
                    foreach($stropheOptions as $stropheOption) {
                        if($verseEnd == $stropheOption->option_values()->where('key', '=', OptionValue::offset)->first()->value) {
                            $isStropheEnd = true;
                            $stropheReadingNumber = $stropheOption->option_values()->where('key', '=', OptionValue::value)->first()->value;
                            break;
                        }
                    }

                    $ret[] = [
                        'value' => $this->replaceEndingCharacters($currentVerse),
                        'isVerseEnd' => true,
                        'verseReadingNumber' => $verseOption->option_values()->where('key', '=', OptionValue::value)->first()->value,
                        'isStropheEnd' => $isStropheEnd,
                        'stropheReadingNumber' => $stropheReadingNumber,
                        'continued' => false,
                        'incipit' => false,
                        'length' => count(splitJunicode($currentVerse)),
                    ];

                    $currentVerse = '';
                }
            }
        }

        if($currentVerse != '') {
            $ret[] = [
                'value' => $this->replaceEndingCharacters($currentVerse),
                'isVerseEnd' => false,
                'verseReadingNumber' => '',
                'isStropheEnd' => false,
                'incipit' => false,
                'stropheReadingNumber' => '',
                'continued' => $hasContinuedOption,
                'length' => count(splitJunicode($currentVerse)),
            ];
        }

        return $ret;
    }

    private function replaceEndingCharacters($verse) {
        // This is done later to avoid problems with the counting
        return $verse;

        $verse = rtrim($verse);

        $verseCharacters = splitJunicode($verse);

        $characters = ['⸗', '~', '·', ':'];
        $replacements = ['', '', '', ''];

        $lastChar = end($verseCharacters);

        foreach($characters as $key => $character) {
            if($lastChar === $character) {
                $verseCharacters[count($verseCharacters) - 1] = $replacements[$key];
            }
        }

        $ret = '';

        foreach($verseCharacters as $char) {
            $ret .= $char;
        }

        return $ret;
    }

    public function replaceAbbreviations(string $verseValue, Verse $verse) {
        $abbreviations = $verse->options()->where('name', '=', Option::abbreviation)->get();

        $searchAndReplace = [];

        foreach($abbreviations as $abbreviation) {
            $replaceWith = $abbreviation->option_values()->where('key', '=', OptionValue::expansion)->first();
            $offset = $abbreviation->option_values()->where('key', '=', OptionValue::offset)->first();
            $length = $abbreviation->option_values()->where('key', '=', OptionValue::length)->first();

            if(!$replaceWith || !$offset || !$length)
                continue;

            $search = substr($verseValue, $offset, $length);

            $searchAndReplace[] = [
                'search' => $search,
                'replace' => $replaceWith,
            ];
        }

        $ret = $verseValue;

        foreach($searchAndReplace as $val) {
            $ret = str_replace($val['search'], $val['replace'], $ret);
        }

        return $ret;
    }
}
