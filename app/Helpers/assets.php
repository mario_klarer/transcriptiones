<?php

if(!function_exists('safeMix')) {
    function safeMix($path): string
    {
        try {
            return mix($path);
        } catch (Throwable $e) {
            return url($path);
        }
    }
}
