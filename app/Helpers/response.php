<?php

if(!function_exists('jsonError')) {
    function jsonError($message = NULL, $error_code = 422): \Illuminate\Http\Response|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory
    {
        if($message === NULL)
            $message = "Error.";

        $error = ['message' => $message];
        return response(json_encode($error), $error_code)->header('Content-Type', 'application/json');
    }
}

if(!function_exists('jsonSuccess')) {
    function jsonSuccess($data = NULL): \Illuminate\Http\JsonResponse
    {
        if($data === NULL)
            $data = "Success!";

        $ret = ['data' => $data];
        return response()->json($ret);
    }
}
