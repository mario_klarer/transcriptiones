<?php

if(!function_exists('splitJunicode')) {
    function splitJunicode(?string $junicode): array {
        return $junicode === NULL ? [] : preg_split('//u', $junicode, null, PREG_SPLIT_NO_EMPTY);
    }
}

if(!function_exists('decodeUnicode')) {
    function decodeUnicode(string $payload): string {
        $array = json_decode('["' . $payload . '"]');
        return array_pop($array);
    }
}

