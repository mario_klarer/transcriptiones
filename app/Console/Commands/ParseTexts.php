<?php

namespace App\Console\Commands;

use App\Jobs\ParseTextsJob;
use Illuminate\Console\Command;

class ParseTexts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:texts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse all texts into Database';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ParseTextsJob::dispatch();

        return Command::SUCCESS;
    }
}
