<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Chapter\ChapterController;
use App\Http\Controllers\Explanation\ShowExplanationController;
use App\Http\Controllers\Home\DashboardController;
use App\Http\Controllers\Legal\LegalController;
use App\Http\Controllers\Page\PageController;
use App\Http\Controllers\Search\SearchController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/imprint', function () {
    return view('presentation.index');
})->name('index');

Route::get('/', function () {
    return view('presentation.index');
})->name('index');

Route::get('/home', DashboardController::class)->name('dashboard');

Route::get('/chapter', ChapterController::class)->name('chapter');

Route::get('/explanation', ShowExplanationController::class)->name('explanation');

Route::get('/logout', LogoutController::class)->name('logout');

Route::get('/page/{page}', PageController::class)->name('page.show');

Route::get('/search', SearchController::class)->name('search');

Route::get('/imprint', [LegalController::class, 'imprint'])->name('imprint');
Route::get('/data-protection', [LegalController::class, 'dataprotection'])->name('dataprotection');
Route::get('/terms', [LegalController::class, 'terms'])->name('terms');

