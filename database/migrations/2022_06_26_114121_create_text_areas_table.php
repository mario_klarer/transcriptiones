<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text_areas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('page_id');
            $table->foreign('page_id')
                ->references('id')
                ->on('pages');
            $table->integer('reading_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_areas');
    }
};
