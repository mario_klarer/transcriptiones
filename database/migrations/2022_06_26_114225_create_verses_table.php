<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('strophe_id')->nullable();
            $table->foreign('strophe_id')
                ->references('id')
                ->on('strophes');
            $table->foreignId('text_area_id');
            $table->foreign('text_area_id')
                ->references('id')
                ->on('text_areas');
            $table->integer('reading_number');
            $table->string('verse_number', 20);
            $table->string('value', 300);
            $table->string('normalized_value', 300);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verses');
    }
};
