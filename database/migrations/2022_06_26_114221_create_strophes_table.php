<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strophes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('text_area_id');
            $table->foreign('text_area_id')
                ->references('id')
                ->on('text_areas');
            $table->integer('reading_number');
            $table->string('strophe_number', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strophes');
    }
};
