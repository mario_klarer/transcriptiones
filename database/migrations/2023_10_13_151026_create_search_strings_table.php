<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_strings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('chapter_id');
            $table->foreign('chapter_id')
                ->references('id')
                ->on('chapters');
            $table->foreignId('page_id');
            $table->foreign('page_id')
                ->references('id')
                ->on('pages');
            $table->string('strophe_number', 20)->nullable();
            $table->string('verse_number', 20);
            $table->string('value', 3000);
            $table->string('normalized_value', 3000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_strings');
    }
};
