<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lines', function (Blueprint $table) {
            $table->integer('min_x')->after('reading_number');
            $table->integer('max_x')->after('reading_number');
            $table->integer('min_y')->after('reading_number');
            $table->integer('max_y')->after('reading_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lines', function (Blueprint $table) {
            $table->dropColumn('min_x');
            $table->dropColumn('max_x');
            $table->dropColumn('min_y');
            $table->dropColumn('max_y');
        });
    }
};
