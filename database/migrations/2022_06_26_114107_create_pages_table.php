<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('chapter_id');
            $table->foreign('chapter_id')
                ->references('id')
                ->on('chapters');
            $table->foreignId('next_page_id')->nullable();
            $table->foreign('next_page_id')
                ->references('id')
                ->on('pages');
            $table->foreignId('previous_page_id')->nullable();
            $table->foreign('previous_page_id')
                ->references('id')
                ->on('pages');
            $table->string('folio', 50)->nullable();
            $table->integer('page_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
};
