<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapters', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->foreignId('next_chapter_id')->nullable();
            $table->foreign('next_chapter_id')
                ->references('id')
                ->on('chapters');
            $table->foreignId('previous_chapter_id')->nullable();
            $table->foreign('previous_chapter_id')
                ->references('id')
                ->on('chapters');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapters');
    }
};
