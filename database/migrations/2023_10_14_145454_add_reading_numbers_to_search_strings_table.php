<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('search_strings', function (Blueprint $table) {
            $table->integer('strophe_reading_number')->after('strophe_number')->nullable();
            $table->integer('verse_reading_number')->after('verse_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('search_strings', function (Blueprint $table) {
            $table->dropColumn('strophe_reading_number');
            $table->dropColumn('verse_reading_number');
        });
    }
};
