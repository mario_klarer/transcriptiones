<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transformation_values', function (Blueprint $table) {
            $table->id();
            $table->string('key', 50);
            $table->string('value', 1000);
            $table->foreignId('transformation_id');
            $table->foreign('transformation_id')
                ->references('id')
                ->on('transformations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transformation_values');
    }
};
