<h1>Transcriptiones</h1>

<h2>Metadata</h2>

<ul>
<li>
<b>Project title:</b> Transcriptiones
</li>

<li>
<b>Principal investigator:</b> Univ.-Prof. Dr. Mario Klarer, University of Innsbruck (https://orcid.org/0000-0003-0712-9328)
</li>

<li>
<b>Funding organization:</b> University of Innsbruck, Research Center Digital Humanities (FZDH)
</li>

<li>
<b>Funding program:</b> DI4DH (Digitization and Information extraction for Digital Humanities)
</li>

<li>
<b>Project website:</b> https://www.transcriptiones.com/
</li>

<li>
<b>License:</b> MIT License
</li>
</ul>

<h2>Required Packages</h2>
<h3>Composer</h3>
<ul>
<li>
php^8.0.2
</li>
<li>
laravel/framework^9.2
</li>
<li>
doctrine/dbal^3.6
</li>
<li>
guzzlehttp/guzzle^7.2
</li>
<li>
laravel/sanctum^2.14.1
</li>
<li>
laravel/tinker^2.7
</li>
<li>
laravel/ui^3.4
</li>
<li>
maatwebsite/excel^3.1
</li>
</ul>
<h3>NPM</h3>
<ul>
<li>
vue^2.6.12
</li>
<li>
vue-loader^15.9.8
</li>
<li>
vue-template-compiler^2.6.12
</li>
<li>
sass^1.32.11
</li>
<li>
sass-loader^11.0.1
</li>
<li>
resolve-url-loader^3.12.1
</li>
<li>
postcss^8.1.14
</li>
<li>
lodash^4.17.19
</li>
<li>
laravel-mix^6.0.6
</li>
<li>
bootstrap^5.1.3
</li>
<li>
axios^0.25
</li>
<li>
@popperjs/core^2.11.4
</li>
<li>
animate.css^4.1.1
</li>
<li>
bootstrap-icons^1.8.1
</li>
<li>
noty^3.2.0-beta-deprecated
</li>
<li>
jquery^3.6.0
</li>
<li>
v-tooltip^2.1.3
</li>
<li>
vue-dragscroll^3.0.1
</li>
</ul>
