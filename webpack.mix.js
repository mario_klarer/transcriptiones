const mix = require('laravel-mix');
const path = require("path");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
        output: { chunkFilename: 'js/[name].js?id=[chunkhash]' },
        resolve: {
            alias: {
                Components: path.resolve('resources/js/components'),
                Modules: path.resolve('resources/js/modules'),
            },
        },
    })
    .js('resources/js/app.js', 'public/js')
    .js('resources/js/vue-init.js', 'public/js')
    .copy('resources/fonts', 'public/fonts')
    .sass('resources/scss/groups/app.scss', 'public/css')
    .sass('resources/scss/groups/explanation.scss', 'public/css')
    .sass('resources/scss/groups/fonts.scss', 'public/css')
    .vue()
    .version()
    .setPublicPath('public');
